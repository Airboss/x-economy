from XPStandardWidgets import *
from XPLMProcessing import *
from XPLMDataAccess import *
from XPLMNavigation import *
from XPLMUtilities import *
from XPLMGraphics import *
from XPWidgetDefs import *
from XPLMDisplay import *
from XPLMMenus import *
from XPWidgets import *
from XPLMPlanes import *
if 64 - 64: i11iIiiIii
from httplib import *
from xml . dom import minidom
from re import *
from math import *
import urllib2
import hashlib
import os
import sys
from urllib import urlopen
if 65 - 65: O0 / iIii1I11I1II1 % OoooooooOO - i1IIi
import platform
if 73 - 73: II111iiii
if 22 - 22: I1IiiI * Oo0Ooo / OoO0O00 . OoOoOO00 . o0oOOo0O0Ooo / I1ii11iIi11i
if 48 - 48: oO0o / OOooOOo / I11i / Ii1I
class engine :
 def __init__ ( self , cht , runtime , chtDamage , mixDamage , engineNumber ) :
  self . defaultcht = cht
  self . runtime = runtime
  self . chtDamage = chtDamage
  self . engineNumber = engineNumber
  self . mixtureDamage = mixDamage
  self . numberOfEngines = XPLMGetDatai ( XPLMFindDataRef ( "sim/aircraft/engine/acf_num_engines" ) )
  print "[XFSE|dbg] Engine created #" + str ( engineNumber )
  if 48 - 48: iII111i % IiII + I1Ii111 / ooOoO0o * Ii1I
 def clearEng ( self ) :
  print "[XFSE|dbg] Clearing engine"
  self . runtime = 0
  self . chtDamage = 0
  self . mixtureDamage = 0
  if 46 - 46: ooOoO0o * I11i - OoooooooOO
 def engineType ( self ) :
  II1iII1i = [ ]
  XPLMGetDatavi ( XPLMFindDataRef ( "sim/aircraft/prop/acf_prop_type" ) , II1iII1i , 0 , self . numberOfEngines )
  return II1iII1i [ self . engineNumber ]
  if 97 - 97: OoooooooOO
 def currentRPM ( self ) :
  Oo0o = [ ]
  XPLMGetDatavf ( XPLMFindDataRef ( "sim/flightmodel/engine/ENGN_N2_" ) , Oo0o , 0 , self . numberOfEngines )
  return Oo0o [ self . engineNumber ]
  if 60 - 60: I1ii11iIi11i + I1Ii111 - I11i / i1IIi
 def currentCHT ( self ) :
  Ii1iI = [ ]
  XPLMGetDatavf ( XPLMFindDataRef ( "sim/flightmodel/engine/ENGN_CHT_c" ) , Ii1iI , 0 , self . numberOfEngines )
  return Ii1iI [ self . engineNumber ]
  if 100 - 100: i1IIi . I1Ii111 / IiII * OoooooooOO + I11i * oO0o
 def currentMIX ( self ) :
  O0IiiiIiI1iIiI1 = [ ]
  XPLMGetDatavf ( XPLMFindDataRef ( "sim/flightmodel/engine/ENGN_mixt" ) , O0IiiiIiI1iIiI1 , 0 , self . numberOfEngines )
  return O0IiiiIiI1iIiI1 [ self . engineNumber ] * 100
  if 85 - 85: OoO0O00
 def planeALT ( self ) :
  iIi1IIii11I = XPLMGetDataf ( XPLMFindDataRef ( "sim/flightmodel/position/y_agl" ) )
  return iIi1IIii11I * float ( 3.33 )
  if 84 - 84: iIii1I11I1II1 . IiII / IiII % IiII
 def feed ( self , sec , rpm , mix , cht , altitude ) :
  if rpm > 0 :
   self . runtime += sec
  if self . defaultcht > 0 :
   i11 = abs ( cht - self . defaultcht ) / float ( sec )
   if i11 > 0 :
    self . chtDamage += i11
  self . defaultcht = cht
  if ( mix > 95 and altitude > 1000 ) :
   self . mixtureDamage += sec
   if 41 - 41: I1Ii111 . ooOoO0o * IiII % i11iIiiIii
 def getData ( self , flightTime ) :
  return "&mixture" + str ( self . engineNumber + 1 ) + "=" + str ( self . mixtureDamage ) + "&heat" + str ( self . engineNumber + 1 ) + "=" + str ( self . chtDamage ) + "&time" + str ( self . engineNumber + 1 ) + "=" + str ( flightTime )
  if 74 - 74: iII111i * IiII
 def isEngRun ( self ) :
  oo00o0Oo0oo = [ ]
  XPLMGetDatavi ( XPLMFindDataRef ( "sim/flightmodel/engine/ENGN_running" ) , oo00o0Oo0oo , 0 , self . numberOfEngines )
  return oo00o0Oo0oo [ self . engineNumber ]
  if 20 - 20: ooOoO0o * II111iiii
  if 65 - 65: o0oOOo0O0Ooo * iIii1I11I1II1 * ooOoO0o
  if 18 - 18: iIii1I11I1II1 / I11i + oO0o / Oo0Ooo - II111iiii - I11i
class PythonInterface :
 def XPluginStart ( self ) :
  self . Name = "X-Economy"
  self . Sig = "ksgy.Python.XFSEconomy"
  self . Desc = "X-Economy - plugin for FSEconomy (www.fseconomy.net)"
  self . VERSION = "1.8.3"
  if 1 - 1: I11i - OOooOOo % O0 + I1IiiI - iII111i / I11i
  print "[XFSE|Nfo] === === === === === === === === === === === === === === === === === === === ==="
  print "[XFSE|Nfo] === " + self . Name + " :: " + self . VERSION + " ==="
  if 31 - 31: OoO0O00 + II111iiii
  print "[XFSE|Dbg] === platform.machine()              " + platform . machine ( )
  print "[XFSE|Dbg] === platform.node()                 " + platform . node ( )
  print "[XFSE|Dbg] === platform.processor()            " + platform . processor ( )
  print "[XFSE|Dbg] === platform.python_compiler()      " + platform . python_compiler ( )
  print "[XFSE|Dbg] === platform.python_implementation()" + platform . python_implementation ( )
  print "[XFSE|Dbg] === platform.python_version()       " + platform . python_version ( )
  print "[XFSE|Dbg] === platform.system()               " + platform . system ( )
  print "[XFSE|Dbg] === platform.release()              " + platform . release ( )
  print "[XFSE|Dbg] === platform.version()              " + platform . version ( )
  print "[XFSE|Nfo] === === === === === === === === === === === === === === === === === === === ==="
  if 13 - 13: OOooOOo * oO0o * I1IiiI
  self . constKgToGal = float ( 2.68735 )
  if 55 - 55: II111iiii
  self . CancelFlyButtonString1 = "Cancel flight"
  self . CancelFlyButtonString2 = "Really sure ?"
  if 43 - 43: OoOoOO00 - i1IIi + I1Ii111 + Ii1I
  self . MenuItem1 = 0
  self . MenuItem2 = 0
  self . cancelCmdFlag = 0
  self . cancelBtnFlag = 0
  if 17 - 17: o0oOOo0O0Ooo
  self . flightTimer = 0
  self . flightTimerLast = 0
  if 64 - 64: Ii1I % i1IIi % OoooooooOO
  self . XPVer = 10
  if 3 - 3: iII111i + O0
  self . connected = 0
  self . flying = 0
  self . airborne = 0
  self . flightStart = 0
  self . flightTime = 0
  self . Arrived = 0
  self . Transmitting = 0
  self . leaseStart = 0
  self . leaseTime = 0
  self . EndFlightCaption = ""
  self . LeaseCaption = 0
  self . CurrentAircraft = ""
  if 42 - 42: OOooOOo / i1IIi + i11iIiiIii - Ii1I
  self . endFlightTime = 60
  self . endPlaneAlt = 10
  self . endPlaneSpd = 3
  self . startPlaneAlt = 20
  self . startPlaneSpd = 15
  self . startPlaneRpm = 10
  self . startBrakeMax = 0.3
  if 78 - 78: OoO0O00
  self . FuelTanks = [ ]
  self . stPayload = 0
  self . stEq = 0
  self . gsCheat = 0
  self . globalX = 0
  self . globalY = 0
  self . checkfuel = 0
  self . errortext = ( [ "" , "" , "" , "" ] )
  self . errorcolor = ""
  self . errormessage = 10
  self . ACEngine = [ ]
  Iii1I111 = XPLMAppendMenuItem ( XPLMFindPluginsMenu ( ) , "X-Economy" , 0 , 1 )
  self . XFSEMenuHandlerCB = self . XFSEMenuHandler
  self . Id = XPLMCreateMenu ( self , "X-Economy" , XPLMFindPluginsMenu ( ) , Iii1I111 , self . XFSEMenuHandlerCB , 0 )
  XPLMAppendMenuItem ( self . Id , "Open X-Economy" , 1 , 1 )
  XPLMAppendMenuItem ( self . Id , "-" , 3 , 1 )
  XPLMAppendMenuItem ( self . Id , "Set aircraft alias" , 2 , 1 )
  self . checkACStateCB = self . checkACState
  XPLMRegisterFlightLoopCallback ( self , self . checkACStateCB , 1.0 , 0 )
  if 60 - 60: oO0o * o0oOOo0O0Ooo % o0oOOo0O0Ooo % I11i * II111iiii + i1IIi
  self . DrawWindowCB = self . DrawWindowCallback
  self . KeyCB = self . KeyCallback
  self . MouseClickCB = self . MouseClickCallback
  self . WindowId = XPLMCreateWindow ( self , 50 , 600 , 300 , 400 , 1 , self . DrawWindowCB , self . KeyCB , self . MouseClickCB , 0 )
  if 64 - 64: oO0o - O0 / II111iiii / o0oOOo0O0Ooo / iIii1I11I1II1
  if 24 - 24: O0 % o0oOOo0O0Ooo + i1IIi + I1Ii111 + I1ii11iIi11i
  self . tempCB0 = self . CallbackDatarefConnected
  self . drConnected = XPLMRegisterDataAccessor ( self , "fse/status/connected" , xplmType_Int , 0 , self . tempCB0 , None , None , None , None , None , None , None , None , None , None , None , 0 , 0 )
  self . tempCB1 = self . CallbackDatarefFlying
  self . drFlying = XPLMRegisterDataAccessor ( self , "fse/status/flying" , xplmType_Int , 0 , self . tempCB1 , None , None , None , None , None , None , None , None , None , None , None , 0 , 0 )
  self . tempCB4 = self . CallbackDatarefAirborne
  self . drAirborne = XPLMRegisterDataAccessor ( self , "fse/status/airborne" , xplmType_Int , 0 , self . tempCB4 , None , None , None , None , None , None , None , None , None , None , None , 0 , 0 )
  self . tempCB2 = self . CallbackDatarefLeasetime
  self . drLeasetime = XPLMRegisterDataAccessor ( self , "fse/status/leasetime" , xplmType_Int , 0 , self . tempCB2 , None , None , None , None , None , None , None , None , None , None , None , 0 , 0 )
  self . tempCB3 = self . CallbackDatarefFlighttime
  self . drFlighttime = XPLMRegisterDataAccessor ( self , "fse/status/flighttime" , xplmType_Int , 0 , self . tempCB3 , None , None , None , None , None , None , None , None , None , None , None , 0 , 0 )
  if 70 - 70: Oo0Ooo % Oo0Ooo . IiII % OoO0O00 * o0oOOo0O0Ooo % oO0o
  if 23 - 23: i11iIiiIii + I1IiiI
  self . CmdServerConn = XPLMCreateCommand ( "fse/server/connect" , "Login to FSE Server" )
  self . CmdWindowShow = XPLMCreateCommand ( "fse/window/show" , "show FSE window" )
  self . CmdWindowHide = XPLMCreateCommand ( "fse/window/hide" , "hide FSE window" )
  self . CmdWindowTogl = XPLMCreateCommand ( "fse/window/toggle" , "toggle FSE window" )
  self . CmdFlightStart = XPLMCreateCommand ( "fse/flight/start" , "Start flight" )
  self . CmdFlightEnd = XPLMCreateCommand ( "fse/flight/finish" , "Finish flight" )
  self . CmdFlightCArm = XPLMCreateCommand ( "fse/flight/cancelArm" , "Cancel flight" )
  self . CmdFlightCCon = XPLMCreateCommand ( "fse/flight/cancelConfirm" , "Cancel flight confirm" )
  if 68 - 68: OoOoOO00 . oO0o . i11iIiiIii
  self . CmdServerConnCB = self . CmdServerConnCallback
  self . CmdWindowShowCB = self . CmdWindowShowCallback
  self . CmdWindowHideCB = self . CmdWindowHideCallback
  self . CmdWindowToglCB = self . CmdWindowToglCallback
  self . CmdFlightStartCB = self . CmdFlightStartCallback
  self . CmdFlightEndCB = self . CmdFlightEndCallback
  self . CmdFlightCArmCB = self . CmdFlightCArmCallback
  self . CmdFlightCConCB = self . CmdFlightCConCallback
  if 40 - 40: oO0o . OoOoOO00 . Oo0Ooo . i1IIi
  XPLMRegisterCommandHandler ( self , self . CmdServerConn , self . CmdServerConnCB , 0 , 0 )
  XPLMRegisterCommandHandler ( self , self . CmdWindowShow , self . CmdWindowShowCB , 0 , 0 )
  XPLMRegisterCommandHandler ( self , self . CmdWindowHide , self . CmdWindowHideCB , 0 , 0 )
  XPLMRegisterCommandHandler ( self , self . CmdWindowTogl , self . CmdWindowToglCB , 0 , 0 )
  XPLMRegisterCommandHandler ( self , self . CmdFlightStart , self . CmdFlightStartCB , 0 , 0 )
  XPLMRegisterCommandHandler ( self , self . CmdFlightEnd , self . CmdFlightEndCB , 0 , 0 )
  XPLMRegisterCommandHandler ( self , self . CmdFlightCArm , self . CmdFlightCArmCB , 0 , 0 )
  XPLMRegisterCommandHandler ( self , self . CmdFlightCCon , self . CmdFlightCConCB , 0 , 0 )
  if 33 - 33: Ii1I + II111iiii % i11iIiiIii . ooOoO0o - I1IiiI
  if 66 - 66: Ii1I - OoooooooOO * OoooooooOO . OOooOOo . I1ii11iIi11i
  self . CreateXFSEWidget ( 220 , 640 , 480 , 490 )
  self . MenuItem1 = 1
  XPHideWidget ( self . XFSEWidget )
  if 22 - 22: OoooooooOO % I11i - iII111i . iIii1I11I1II1 * i11iIiiIii
  self . CreateACAliasWidget ( 720 , 760 , 272 , 87 )
  self . MenuItem2 = 1
  XPHideWidget ( self . ACAliasWidget )
  if 32 - 32: Oo0Ooo * O0 % oO0o % Ii1I . IiII
  if 61 - 61: ooOoO0o
  return self . Name , self . Sig , self . Desc
  if 79 - 79: Oo0Ooo + I1IiiI - iII111i
 def XPluginStop ( self ) :
  if ( self . MenuItem1 == 1 ) :
   XPDestroyWidget ( self , self . XFSEWidget , 1 )
   self . MenuItem1 = 0
  if ( self . MenuItem2 == 1 ) :
   XPDestroyWidget ( self , self . ACAliasWidget , 1 )
   self . MenuItem2 = 0
   if 83 - 83: ooOoO0o
  XPLMDestroyMenu ( self , self . Id )
  XPLMUnregisterFlightLoopCallback ( self , self . checkACStateCB , 0 )
  XPLMDestroyWindow ( self , self . WindowId )
  if 64 - 64: OoO0O00 % ooOoO0o % iII111i / OoOoOO00 - OoO0O00
  XPLMUnregisterDataAccessor ( self , self . drConnected )
  XPLMUnregisterDataAccessor ( self , self . drFlying )
  XPLMUnregisterDataAccessor ( self , self . drAirborne )
  XPLMUnregisterDataAccessor ( self , self . drLeasetime )
  XPLMUnregisterDataAccessor ( self , self . drFlighttime )
  if 74 - 74: iII111i * O0
  XPLMUnregisterCommandHandler ( self , self . CmdServerConn , self . CmdServerConnCB , 0 , 0 )
  XPLMUnregisterCommandHandler ( self , self . CmdWindowShow , self . CmdWindowShowCB , 0 , 0 )
  XPLMUnregisterCommandHandler ( self , self . CmdWindowHide , self . CmdWindowHideCB , 0 , 0 )
  XPLMUnregisterCommandHandler ( self , self . CmdWindowTogl , self . CmdWindowToglCB , 0 , 0 )
  XPLMUnregisterCommandHandler ( self , self . CmdFlightStart , self . CmdFlightStartCB , 0 , 0 )
  XPLMUnregisterCommandHandler ( self , self . CmdFlightEnd , self . CmdFlightEndCB , 0 , 0 )
  XPLMUnregisterCommandHandler ( self , self . CmdFlightCArm , self . CmdFlightCArmCB , 0 , 0 )
  XPLMUnregisterCommandHandler ( self , self . CmdFlightCCon , self . CmdFlightCConCB , 0 , 0 )
  if 89 - 89: oO0o + Oo0Ooo
  pass
  if 3 - 3: i1IIi / I1IiiI % I11i * i11iIiiIii / O0 * I11i
 def XPluginEnable ( self ) :
  return 1
  if 49 - 49: oO0o % Ii1I + i1IIi . I1IiiI % I1ii11iIi11i
 def XPluginDisable ( self ) :
  pass
  if 48 - 48: I11i + I11i / II111iiii / iIii1I11I1II1
 def XPluginReceiveMessage ( self , inFromWho , inMessage , inParam ) :
  pass
  if 20 - 20: o0oOOo0O0Ooo
  if 77 - 77: OoOoOO00 / I11i
  if 98 - 98: iIii1I11I1II1 / i1IIi / i11iIiiIii / o0oOOo0O0Ooo
 def CallbackDatarefConnected ( self , inval ) :
  return self . connected
 def CallbackDatarefFlying ( self , inval ) :
  return self . flying
 def CallbackDatarefAirborne ( self , inval ) :
  return self . airborne
 def CallbackDatarefLeasetime ( self , inval ) :
  return self . leaseTime
 def CallbackDatarefFlighttime ( self , inval ) :
  return self . flightTime
  if 28 - 28: OOooOOo - IiII . IiII + OoOoOO00 - OoooooooOO + O0
  if 95 - 95: OoO0O00 % oO0o . O0
  if 15 - 15: ooOoO0o / Ii1I . Ii1I - i1IIi
 def CmdServerConnCallback ( self , cmd , phase , refcon ) :
  if ( phase == 0 ) :
   print "[XFSE|Nfo] CMD server connect"
   self . login ( )
  return 0
  if 53 - 53: IiII + I1IiiI * oO0o
 def CmdWindowShowCallback ( self , cmd , phase , refcon ) :
  if ( phase == 0 ) :
   print "[XFSE|Nfo] CMD window show"
   XPShowWidget ( self . XFSEWidget )
  return 0
  if 61 - 61: i1IIi * OOooOOo / OoooooooOO . i11iIiiIii . OoOoOO00
 def CmdWindowHideCallback ( self , cmd , phase , refcon ) :
  if ( phase == 0 ) :
   print "[XFSE|Nfo] CMD window hide"
   XPHideWidget ( self . XFSEWidget )
  return 0
  if 60 - 60: I11i / I11i
 def CmdWindowToglCallback ( self , cmd , phase , refcon ) :
  if ( phase == 0 ) :
   print "[XFSE|Nfo] CMD window toggle"
   if ( not XPIsWidgetVisible ( self . XFSEWidget ) ) :
    XPShowWidget ( self . XFSEWidget )
   else :
    XPHideWidget ( self . XFSEWidget )
  return 0
  if 46 - 46: Ii1I * OOooOOo - OoO0O00 * oO0o - I1Ii111
 def CmdFlightStartCallback ( self , cmd , phase , refcon ) :
  if ( phase == 0 ) :
   print "[XFSE|Nfo] CMD flight start"
   self . startFly ( )
  return 0
  if 83 - 83: OoooooooOO
 def CmdFlightEndCallback ( self , cmd , phase , refcon ) :
  if ( phase == 0 ) :
   print "[XFSE|Nfo] CMD flight end"
   if 31 - 31: II111iiii - OOooOOo . I1Ii111 % OoOoOO00 - O0
   self . endFly ( )
  return 0
  if 4 - 4: II111iiii / ooOoO0o . iII111i
 def CmdFlightCArmCallback ( self , cmd , phase , refcon ) :
  if ( phase == 0 ) :
   print "[XFSE|Nfo] CMD flight cancel arm"
   self . cancelCmdFlag = 1
  return 0
  if 58 - 58: OOooOOo * i11iIiiIii / OoOoOO00 % I1Ii111 - I1ii11iIi11i / oO0o
 def CmdFlightCConCallback ( self , cmd , phase , refcon ) :
  if ( phase == 0 ) :
   if ( self . cancelCmdFlag == 1 ) :
    print "[XFSE|Nfo] CMD flight cancel confirm"
    self . cancelFlight ( "Flight cancelled" , "" )
   else :
    print "[XFSE|Nfo] CMD flight cancel confirm is locked!"
  return 0
  if 50 - 50: I1IiiI
  if 34 - 34: I1IiiI * II111iiii % iII111i * OoOoOO00 - I1IiiI
  if 33 - 33: o0oOOo0O0Ooo + OOooOOo * OoO0O00 - Oo0Ooo / oO0o % Ii1I
 def MouseClickCallback ( self , inWindowID , x , y , inMouse , inRefcon ) :
  return 0
  if 21 - 21: OoO0O00 * iIii1I11I1II1 % oO0o * i1IIi
 def KeyCallback ( self , inWindowID , inKey , inFlags , inVirtualKey , inRefcon , losingFocus ) :
  return 0
  if 16 - 16: O0 - I1Ii111 * iIii1I11I1II1 + iII111i
 def DrawWindowCallback ( self , inWindowID , inRefcon ) :
  if ( self . errortext [ 0 ] != "" and self . errormessage > 0 ) :
   Ii11iII1 = [ ] ; Oo0O0O0ooO0O = [ ] ; IIIIii = [ ] ; O0o0 = [ ]
   XPLMGetWindowGeometry ( inWindowID , Ii11iII1 , Oo0O0O0ooO0O , IIIIii , O0o0 )
   OO00Oo = int ( Ii11iII1 [ 0 ] ) ; O0OOO0OOoO0O = int ( Oo0O0O0ooO0O [ 0 ] ) ; O00Oo000ooO0 = int ( IIIIii [ 0 ] ) ; OoO0O00IIiII = int ( O0o0 [ 0 ] )
   if 80 - 80: IiII . oO0o
   if 25 - 25: OoOoOO00 . II111iiii / iII111i . OOooOOo * OoO0O00 . I1IiiI
   Oo0oOOo = 275
   try :
    if ( self . errortext [ 3 ] == "" ) :
     Oo0oOOo = 290
     if ( self . errortext [ 2 ] == "" ) :
      Oo0oOOo = 305
      if ( self . errortext [ 1 ] == "" ) :
       Oo0oOOo = 315
   except TypeError :
    print "[XFSE|ERR] Error creating text height ... using default"
    Oo0oOOo = 275
    if 58 - 58: II111iiii * OOooOOo * I1ii11iIi11i / OOooOOo
   try :
    if 75 - 75: oO0o
    I1III = 0
    for OO0O0OoOO0 in self . errortext :
     iiiI1I11i1 = XPLMMeasureString ( xplmFont_Basic , OO0O0OoOO0 , len ( OO0O0OoOO0 ) ) + 20
     if ( iiiI1I11i1 > I1III ) :
      I1III = iiiI1I11i1
   except TypeError :
    print "[XFSE|ERR] Error creating text width ... using default"
    I1III = 130
    if 49 - 49: I1IiiI % ooOoO0o . ooOoO0o . I11i * ooOoO0o
   XPLMDrawTranslucentDarkBox ( OO00Oo , O0OOO0OOoO0O + 150 , O00Oo000ooO0 + I1III - 250 , OoO0O00IIiII + Oo0oOOo )
   XPLMDrawTranslucentDarkBox ( OO00Oo , O0OOO0OOoO0O + 150 , O00Oo000ooO0 + I1III - 250 , OoO0O00IIiII + Oo0oOOo )
   if 97 - 97: Ii1I + o0oOOo0O0Ooo . OOooOOo + I1ii11iIi11i % iII111i
   oo0O = 0.2
   o0 = 1.0 , 1.0 , 1.0
   if ( self . errorcolor == "green" ) :
    o0 = oo0O , 1.0 , oo0O
   if ( self . errorcolor == "red" ) :
    o0 = 1.0 , oo0O , oo0O
   if ( self . errorcolor == "yellow" ) :
    o0 = 1.0 , 1.0 , oo0O
    if 91 - 91: iIii1I11I1II1 + I1Ii111
   XPLMDrawString ( o0 , OO00Oo + 10 , O0OOO0OOoO0O + 132 , self . errortext [ 0 ] , 0 , xplmFont_Basic )
   XPLMDrawString ( o0 , OO00Oo + 10 , O0OOO0OOoO0O + 117 , self . errortext [ 1 ] , 0 , xplmFont_Basic )
   XPLMDrawString ( o0 , OO00Oo + 10 , O0OOO0OOoO0O + 102 , self . errortext [ 2 ] , 0 , xplmFont_Basic )
   XPLMDrawString ( o0 , OO00Oo + 10 , O0OOO0OOoO0O + 87 , self . errortext [ 3 ] , 0 , xplmFont_Basic )
   if 31 - 31: IiII . OoOoOO00 . OOooOOo
   if 75 - 75: I11i + OoO0O00 . OoOoOO00 . ooOoO0o + Oo0Ooo . OoO0O00
   if 96 - 96: OOooOOo . ooOoO0o - Oo0Ooo + iIii1I11I1II1 / OoOoOO00 * OOooOOo
 def CreateXFSEWidget ( self , x , y , w , h ) :
  if 65 - 65: Ii1I . iIii1I11I1II1 / O0 - Ii1I
  try :
   iii1i1iiiiIi = open ( os . path . join ( 'Resources' , 'plugins' , 'PythonScripts' , 'x-economy.ini' ) , 'r' )
   Iiii = iii1i1iiiiIi . readline ( )
   Iiii = Iiii . replace ( '\n' , '' )
   OO0OoO0o00 = iii1i1iiiiIi . readline ( )
   iii1i1iiiiIi . close ( )
   print "[XFSE|dbg] Init successfully completed"
   if 53 - 53: O0 * OoO0O00 + OOooOOo
  except IOError , ( Ii , oOOo0 ) :
   Iiii = ""
   OO0OoO0o00 = ""
   if 54 - 54: O0 - IiII % OOooOOo
  self . globalX = x
  self . globalY = y
  OOoO = x + w
  iII = y - h
  if 38 - 38: I1Ii111
  if 7 - 7: O0 . iII111i % I1ii11iIi11i - I1IiiI - iIii1I11I1II1
  if 36 - 36: IiII % ooOoO0o % Oo0Ooo - I1ii11iIi11i
  if 22 - 22: iIii1I11I1II1 / Oo0Ooo * I1ii11iIi11i % iII111i
  if 85 - 85: oO0o % i11iIiiIii - iII111i * OoooooooOO / I1IiiI % I1IiiI
  IIiIi1iI = [ ]
  XPLMGetDatab ( XPLMFindDataRef ( "sim/version/sim_build_string" ) , IIiIi1iI , 0 , 30 )
  if " 2011 " in str ( IIiIi1iI ) :
   self . XPVer = 9
  else :
   self . XPVer = 10
   if 35 - 35: Ii1I % O0 - O0
   if 16 - 16: II111iiii % OoOoOO00 - II111iiii + Ii1I
   if 12 - 12: OOooOOo / OOooOOo + i11iIiiIii
   if 40 - 40: I1IiiI . iIii1I11I1II1 / I1IiiI / i11iIiiIii
   if 75 - 75: I11i + o0oOOo0O0Ooo
   if 84 - 84: IiII . i11iIiiIii . IiII * I1ii11iIi11i - I11i
   if 42 - 42: i11iIiiIii
   if 33 - 33: iII111i - O0 * i1IIi * o0oOOo0O0Ooo - Oo0Ooo
  iiIiI = "X-Economy v" + str ( self . VERSION ) + " for XP" + str ( self . XPVer ) + " on " + platform . system ( )
  if 91 - 91: iII111i % i1IIi % iIii1I11I1II1
  if 20 - 20: OOooOOo % Ii1I / Ii1I + Ii1I
  self . XFSEWidget = XPCreateWidget ( x , y , OOoO , iII , 1 , iiIiI , 1 , 0 , xpWidgetClass_MainWindow )
  if 45 - 45: oO0o - IiII - OoooooooOO - OoO0O00 . II111iiii / O0
  if 51 - 51: O0 + iII111i
  XPSetWidgetProperty ( self . XFSEWidget , xpProperty_MainWindowHasCloseBoxes , 1 )
  if 8 - 8: oO0o * OoOoOO00 - Ii1I - OoO0O00 * OOooOOo % I1IiiI
  if 48 - 48: O0
  if 11 - 11: I11i + OoooooooOO - OoO0O00 / o0oOOo0O0Ooo + Oo0Ooo . II111iiii
  i1Iii1i1I = XPCreateWidget ( x + 10 , y - 30 , OOoO - 10 , iII + 400 ,
 1 ,
  "" ,
  0 ,
  self . XFSEWidget ,
 xpWidgetClass_SubWindow )
  if 91 - 91: I1ii11iIi11i + I1IiiI . OOooOOo * I1ii11iIi11i + I1IiiI * Oo0Ooo
  if 80 - 80: iII111i % OOooOOo % oO0o - Oo0Ooo + Oo0Ooo
  XPSetWidgetProperty ( i1Iii1i1I , xpProperty_SubWindowType , xpSubWindowStyle_SubWindow )
  if 19 - 19: OoOoOO00 * i1IIi
  if 14 - 14: iII111i
  I1iI1iIi111i = XPCreateWidget ( x + 10 , y - 100 , OOoO - 10 , iII + 10 ,
 1 ,
  "" ,
  0 ,
  self . XFSEWidget ,
 xpWidgetClass_SubWindow )
  if 44 - 44: i1IIi % II111iiii + I11i
  if 45 - 45: iII111i / iII111i + I1Ii111 + ooOoO0o
  XPSetWidgetProperty ( I1iI1iIi111i , xpProperty_SubWindowType , xpSubWindowStyle_SubWindow )
  if 47 - 47: o0oOOo0O0Ooo + ooOoO0o
  OoO = XPCreateWidget ( x + 15 , y - 130 , OOoO - 15 , iII + 150 ,
 1 ,
  "" ,
  0 ,
  self . XFSEWidget ,
 xpWidgetClass_SubWindow )
  if 88 - 88: iII111i . II111iiii * II111iiii % I1Ii111
  if 15 - 15: i1IIi * I1IiiI + i11iIiiIii
  XPSetWidgetProperty ( I1iI1iIi111i , xpProperty_SubWindowType , xpSubWindowStyle_SubWindow )
  if 6 - 6: ooOoO0o / i11iIiiIii + iII111i * oO0o
  if 80 - 80: II111iiii
  O0O = XPCreateWidget ( x + 20 , y - 40 , x + 50 , y - 60 , 1 , "Username:" , 0 , self . XFSEWidget , xpWidgetClass_Caption )
  if 1 - 1: II111iiii
  if 84 - 84: o0oOOo0O0Ooo % II111iiii . i11iIiiIii / OoO0O00
  self . LoginUserEdit = XPCreateWidget ( x + 80 , y - 40 , x + 160 , y - 60 , 1 , Iiii , 0 , self . XFSEWidget , xpWidgetClass_TextField )
  XPSetWidgetProperty ( self . LoginUserEdit , xpProperty_TextFieldType , xpTextEntryField )
  XPSetWidgetProperty ( self . LoginUserEdit , xpProperty_Enabled , 1 )
  if 80 - 80: I1Ii111 . i11iIiiIii - o0oOOo0O0Ooo
  if 25 - 25: OoO0O00
  oOo0oO = XPCreateWidget ( x + 20 , y - 60 , x + 50 , y - 80 , 1 , "Password:" , 0 , self . XFSEWidget , xpWidgetClass_Caption )
  if 51 - 51: Oo0Ooo - oO0o + II111iiii * Ii1I . I11i + oO0o
  if 78 - 78: i11iIiiIii / iII111i - Ii1I / OOooOOo + oO0o
  self . LoginPassEdit = XPCreateWidget ( x + 80 , y - 60 , x + 160 , y - 80 , 1 , OO0OoO0o00 , 0 , self . XFSEWidget , xpWidgetClass_TextField )
  XPSetWidgetProperty ( self . LoginPassEdit , xpProperty_TextFieldType , xpTextEntryField )
  XPSetWidgetProperty ( self . LoginPassEdit , xpProperty_Enabled , 1 )
  XPSetWidgetProperty ( self . LoginPassEdit , xpProperty_PasswordMode , 1 )
  if 82 - 82: Ii1I
  if 46 - 46: OoooooooOO . i11iIiiIii
  self . LoginButton = XPCreateWidget ( x + 180 , y - 40 , x + 260 , y - 60 , 1 , "Log in" , 0 , self . XFSEWidget , xpWidgetClass_Button )
  XPSetWidgetProperty ( self . LoginButton , xpProperty_ButtonType , xpPushButton )
  if 94 - 94: o0oOOo0O0Ooo * Ii1I / Oo0Ooo / Ii1I
  if 87 - 87: Oo0Ooo . IiII
  self . ServerResponseCaption = XPCreateWidget ( x + 180 , y - 60 , x + 260 , y - 80 , 1 , "Not logged in" , 0 , self . XFSEWidget , xpWidgetClass_Caption )
  if 75 - 75: ooOoO0o + OoOoOO00 + o0oOOo0O0Ooo * I11i % oO0o . iII111i
  if 55 - 55: OOooOOo . I1IiiI
  self . AssignmentListCaption = XPCreateWidget ( x + 20 , y - 105 , x + 50 , y - 125 , 1 , "Assignment info:" , 0 , self . XFSEWidget , xpWidgetClass_Caption )
  if 61 - 61: Oo0Ooo % IiII . Oo0Ooo
  if 100 - 100: I1Ii111 * O0
  self . ErrorCaption = [ ]
  self . ErrorCaption . append ( XPCreateWidget ( x + 20 , y - 410 , x + 50 , y - 430 , 1 , "" , 0 , self . XFSEWidget , xpWidgetClass_Caption ) )
  if 64 - 64: OOooOOo % iIii1I11I1II1 * oO0o
  self . ErrorCaption . append ( XPCreateWidget ( x + 20 , y - 425 , x + 50 , y - 445 , 1 , "" , 0 , self . XFSEWidget , xpWidgetClass_Caption ) )
  if 79 - 79: O0
  self . ErrorCaption . append ( XPCreateWidget ( x + 20 , y - 440 , x + 50 , y - 460 , 1 , "" , 0 , self . XFSEWidget , xpWidgetClass_Caption ) )
  if 78 - 78: I1ii11iIi11i + OOooOOo - I1Ii111
  self . ErrorCaption . append ( XPCreateWidget ( x + 20 , y - 455 , x + 50 , y - 475 , 1 , "" , 0 , self . XFSEWidget , xpWidgetClass_Caption ) )
  if 38 - 38: o0oOOo0O0Ooo - oO0o + iIii1I11I1II1 / OoOoOO00 % Oo0Ooo
  if 57 - 57: OoO0O00 / ooOoO0o
  self . FromCaption = [ ]
  self . ToCaption = [ ]
  self . CargoCaption = [ ]
  if 29 - 29: iIii1I11I1II1 + OoOoOO00 * OoO0O00 * OOooOOo . I1IiiI * I1IiiI
  if 7 - 7: IiII * I1Ii111 % Ii1I - o0oOOo0O0Ooo
  self . ACRegCaption = XPCreateWidget ( x + 20 , y - 340 , x + 50 , y - 360 , 1 , "" , 0 , self . XFSEWidget , xpWidgetClass_Caption )
  if 13 - 13: Ii1I . i11iIiiIii
  if 56 - 56: I1ii11iIi11i % O0 - I1IiiI
  self . LeaseCaption = XPCreateWidget ( x + 20 , y - 360 , x + 50 , y - 380 , 1 , "" , 0 , self . XFSEWidget , xpWidgetClass_Caption )
  if 100 - 100: Ii1I - O0 % oO0o * OOooOOo + I1IiiI
  if 88 - 88: OoooooooOO - OoO0O00 * O0 * OoooooooOO . OoooooooOO
  self . EndFlightCaption = XPCreateWidget ( x + 20 , y - 330 , x + 50 , y - 450 , 1 , "" , 0 , self . XFSEWidget , xpWidgetClass_Caption )
  if 33 - 33: I1Ii111 + iII111i * oO0o / iIii1I11I1II1 - I1IiiI
  if 54 - 54: I1Ii111 / OOooOOo . oO0o % iII111i
  self . StartFlyButton = XPCreateWidget ( x + 360 , y - 40 , x + 450 , y - 60 ,
 1 , "Start flight" , 0 , self . XFSEWidget ,
 xpWidgetClass_Button )
  XPSetWidgetProperty ( self . StartFlyButton , xpProperty_ButtonType , xpPushButton )
  XPSetWidgetProperty ( self . StartFlyButton , xpProperty_Enabled , 0 )
  if 57 - 57: i11iIiiIii . I1ii11iIi11i - Ii1I - oO0o + OoOoOO00
  self . EndFlyButton = XPCreateWidget ( x + 360 , y - 20 , x + 450 , y - 80 ,
 1 , "Finish flight" , 0 , self . XFSEWidget ,
 xpWidgetClass_Button )
  XPSetWidgetProperty ( self . EndFlyButton , xpProperty_ButtonType , xpPushButton )
  XPSetWidgetProperty ( self . EndFlyButton , xpProperty_Enabled , 0 )
  XPHideWidget ( self . EndFlyButton )
  if 63 - 63: OoOoOO00 * iII111i
  if 69 - 69: O0 . OoO0O00
  self . CancelFlyButton = XPCreateWidget ( x + 360 , y - 60 , x + 450 , y - 80 ,
 1 , self . CancelFlyButtonString1 , 0 , self . XFSEWidget ,
 xpWidgetClass_Button )
  XPSetWidgetProperty ( self . CancelFlyButton , xpProperty_ButtonType , xpPushButton )
  XPSetWidgetProperty ( self . CancelFlyButton , xpProperty_Enabled , 0 )
  if 49 - 49: I1IiiI - I11i
  if 74 - 74: iIii1I11I1II1 * I1ii11iIi11i + OoOoOO00 / i1IIi / II111iiii . Oo0Ooo
  self . XFSEHandlerCB = self . XFSEHandler
  XPAddWidgetCallback ( self , self . XFSEWidget , self . XFSEHandlerCB )
  if 62 - 62: OoooooooOO * I1IiiI
  if 58 - 58: OoOoOO00 % o0oOOo0O0Ooo
  self . UpdateButton = XPCreateWidget ( x + 270 , y - 40 , x + 350 , y - 60 , 1 , "Update" , 0 , self . XFSEWidget , xpWidgetClass_Button )
  XPSetWidgetProperty ( self . UpdateButton , xpProperty_ButtonType , xpPushButton )
  XPSetWidgetProperty ( self . UpdateButton , xpProperty_Enabled , 0 )
  XPHideWidget ( self . UpdateButton )
  if 50 - 50: I1Ii111 . o0oOOo0O0Ooo
  if 97 - 97: O0 + OoOoOO00
  if 89 - 89: o0oOOo0O0Ooo + OoO0O00 * I11i * Ii1I
 def XFSEHandler ( self , inMessage , inWidget , inParam1 , inParam2 ) :
  if ( inMessage == xpMessage_CloseButtonPushed ) :
   print "[XFSE|dbg] Client window closed"
   if ( self . MenuItem1 == 1 ) :
    XPHideWidget ( self . XFSEWidget )
    return 1
    if 37 - 37: OoooooooOO - O0 - o0oOOo0O0Ooo
  if ( inMessage == xpMsg_PushButtonPressed ) :
   if ( inParam1 == self . LoginButton ) :
    print "[XFSE|Nfo] BTN Login"
    self . login ( )
    return 1
   elif ( inParam1 == self . StartFlyButton ) :
    print "[XFSE|Nfo] BTN Start flying"
    self . startFly ( )
    return 1
   elif ( inParam1 == self . EndFlyButton ) :
    print "[XFSE|Nfo] BTN End flying"
    self . endFly ( )
    return 1
   elif ( inParam1 == self . CancelFlyButton ) :
    print "[XFSE|Nfo] BTN cancel flight"
    if ( self . cancelBtnFlag == 0 ) :
     self . cancelBtnFlag = 1
     XPSetWidgetDescriptor ( self . CancelFlyButton , self . CancelFlyButtonString2 )
    else :
     self . cancelFlight ( "Flight cancelled" , "" )
    return 1
   elif ( inParam1 == self . UpdateButton ) :
    self . doUpdate ( )
    return 1
   else :
    print "[XFSE|ERR] UNKNOWN GUI button pressed"
    if 77 - 77: OOooOOo * iIii1I11I1II1
  return 0
  if 98 - 98: I1IiiI % Ii1I * OoooooooOO
  if 51 - 51: iIii1I11I1II1 . OoOoOO00 / oO0o + o0oOOo0O0Ooo
  if 33 - 33: ooOoO0o . II111iiii % iII111i + o0oOOo0O0Ooo
 def ReadFuelTankConfFile ( self ) :
  oO00O000oO0 = XPLMGetNthAircraftModel ( 0 )
  O0OoOO0o = os . path . dirname ( oO00O000oO0 [ 1 ] )
  ooooo0O0000oo = os . path . join ( O0OoOO0o , 'xfse_fueltanks.txt' )
  if 21 - 21: o0oOOo0O0Ooo - I1IiiI
  if ( os . path . exists ( ooooo0O0000oo ) and os . path . isfile ( ooooo0O0000oo ) ) :
   with open ( ooooo0O0000oo , "r" ) as II11I1 :
    oOo0 = [ ]
    for i1iI in II11I1 :
     i1iI = i1iI . strip ( )
     if ( i1iI . find ( "#" ) != 0 ) :
      if ( len ( i1iI ) == 1 or len ( i1iI ) == 3 ) :
       oOo0 . append ( i1iI )
   return oOo0
  else :
   return ""
   if 94 - 94: iIii1I11I1II1 / Oo0Ooo % iII111i * iII111i * II111iiii
   if 29 - 29: OoO0O00 + OoOoOO00 / o0oOOo0O0Ooo / OOooOOo * iIii1I11I1II1
   if 62 - 62: OOooOOo / oO0o - OoO0O00 . I11i
   if 11 - 11: I1ii11iIi11i . OoO0O00 * IiII * OoooooooOO + ooOoO0o
 def ReadACAliasFromFile ( self ) :
  oO00O000oO0 = XPLMGetNthAircraftModel ( 0 )
  O0OoOO0o = os . path . dirname ( oO00O000oO0 [ 1 ] )
  IiII111i1i11 = os . path . join ( O0OoOO0o , 'xfse_alias.txt' )
  if 40 - 40: ooOoO0o * IiII * i11iIiiIii
  if ( os . path . exists ( IiII111i1i11 ) and os . path . isfile ( IiII111i1i11 ) ) :
   oo0OO00OoooOo = open ( IiII111i1i11 , 'r' )
   II1i111Ii1i = oo0OO00OoooOo . readline ( )
   oo0OO00OoooOo . close ( )
   II1i111Ii1i = II1i111Ii1i . replace ( '\r' , '' )
   II1i111Ii1i = II1i111Ii1i . replace ( '\n' , '' )
   return II1i111Ii1i
  return ""
  if 15 - 15: II111iiii / i1IIi
 def WriteACAliasToFile ( self , alias ) :
  oO00O000oO0 = XPLMGetNthAircraftModel ( 0 )
  O0OoOO0o = os . path . dirname ( oO00O000oO0 [ 1 ] )
  IiII111i1i11 = os . path . join ( O0OoOO0o , 'xfse_alias.txt' )
  oo0OO00OoooOo = open ( IiII111i1i11 , 'wb' )
  alias = oo0OO00OoooOo . write ( alias )
  oo0OO00OoooOo . close ( )
  if 76 - 76: I11i / OOooOOo . O0 % I1IiiI . o0oOOo0O0Ooo + IiII
 def ACAliasWidget_cb ( self , inMessage , inWidget , inParam1 , inParam2 ) :
  if ( inMessage == xpMessage_CloseButtonPushed ) :
   XPHideWidget ( self . ACAliasWidget )
   return 1
   if 71 - 71: I1Ii111 . II111iiii
  if ( inMessage == xpMsg_PushButtonPressed ) :
   if ( inParam1 == self . SetACAliasButton ) :
    oo0 = [ ]
    XPGetWidgetDescriptor ( self . ACAliasEdit , oo0 , 256 )
    self . WriteACAliasToFile ( oo0 [ 0 ] )
    XPHideWidget ( self . ACAliasWidget )
    return 1
    if 61 - 61: OoOoOO00 - OOooOOo - i1IIi
  if ( inMessage == xpMsg_Shown ) :
   oo0 = self . ReadACAliasFromFile ( )
   XPSetWidgetDescriptor ( self . ACAliasEdit , oo0 )
   return 1
   if 25 - 25: O0 * I11i + I1ii11iIi11i . o0oOOo0O0Ooo . o0oOOo0O0Ooo
  return 0
  if 58 - 58: I1IiiI
 def CreateACAliasWidget ( self , x , y , w , h ) :
  OOoO = x + w
  iII = y - h
  if 53 - 53: i1IIi
  self . ACAliasWidget = XPCreateWidget ( x , y , OOoO , iII , 1 , "Enter custom alias" , 1 , 0 , xpWidgetClass_MainWindow )
  XPSetWidgetProperty ( self . ACAliasWidget , xpProperty_MainWindowHasCloseBoxes , 1 )
  if 59 - 59: o0oOOo0O0Ooo
  oOoO00O0 = XPCreateWidget ( x + 7 , y - 17 , x + 248 , y - 37 , 1 , "Leave input field blank to use alias from .acf file" , 0 , self . ACAliasWidget , xpWidgetClass_Caption )
  if 75 - 75: I1IiiI . ooOoO0o . O0 * I1Ii111
  if 4 - 4: Ii1I % oO0o * OoO0O00
  oo0 = self . ReadACAliasFromFile ( )
  self . ACAliasEdit = XPCreateWidget ( x + 7 , y - 40 , x + 265 , y - 60 , 1 , oo0 , 0 , self . ACAliasWidget , xpWidgetClass_TextField )
  XPSetWidgetProperty ( self . ACAliasEdit , xpProperty_TextFieldType , xpTextEntryField )
  XPSetWidgetProperty ( self . ACAliasEdit , xpProperty_Enabled , 1 )
  if 100 - 100: I1Ii111 * OOooOOo + OOooOOo
  if 54 - 54: OoooooooOO + o0oOOo0O0Ooo - i1IIi % i11iIiiIii
  self . SetACAliasButton = XPCreateWidget ( x + 96 , y - 62 , x + 176 , y - 82 , 1 , "Set" , 0 , self . ACAliasWidget , xpWidgetClass_Button )
  XPSetWidgetProperty ( self . SetACAliasButton , xpProperty_ButtonType , xpPushButton )
  if 3 - 3: o0oOOo0O0Ooo % o0oOOo0O0Ooo
  if 83 - 83: II111iiii + I1Ii111
  self . ACAliasWidgetCB = self . ACAliasWidget_cb
  XPAddWidgetCallback ( self , self . ACAliasWidget , self . ACAliasWidgetCB )
  if 73 - 73: iII111i
  if 42 - 42: i11iIiiIii * iIii1I11I1II1 / I1ii11iIi11i . i11iIiiIii % I11i
  if 41 - 41: IiII / O0
  if 51 - 51: I11i % I1IiiI
  if 60 - 60: I1IiiI / OOooOOo . I1IiiI / I1Ii111 . IiII
  if 92 - 92: OoOoOO00 + I1Ii111 * Ii1I % I1IiiI
  if 42 - 42: Oo0Ooo
  if 76 - 76: I1IiiI * iII111i % I1Ii111
  if 57 - 57: iIii1I11I1II1 - i1IIi / I1Ii111 - O0 * OoooooooOO % II111iiii
  if 68 - 68: OoooooooOO * I11i % OoOoOO00 - IiII
  if 34 - 34: I1Ii111 . iIii1I11I1II1 * OoOoOO00 * oO0o / I1Ii111 / I1ii11iIi11i
  if 78 - 78: Oo0Ooo - o0oOOo0O0Ooo / OoOoOO00
  if 10 - 10: iII111i + Oo0Ooo * I1ii11iIi11i + iIii1I11I1II1 / I1Ii111 / I1ii11iIi11i
  if 42 - 42: I1IiiI
  if 38 - 38: OOooOOo + II111iiii % ooOoO0o % OoOoOO00 - Ii1I / OoooooooOO
  if 73 - 73: o0oOOo0O0Ooo * O0 - i11iIiiIii
  if 85 - 85: Ii1I % iII111i + I11i / o0oOOo0O0Ooo . oO0o + OOooOOo
  if 62 - 62: i11iIiiIii + i11iIiiIii - o0oOOo0O0Ooo
  if 28 - 28: iII111i . iII111i % iIii1I11I1II1 * iIii1I11I1II1 . o0oOOo0O0Ooo / iII111i
  if 27 - 27: OoO0O00 + ooOoO0o - i1IIi
  if 69 - 69: IiII - O0 % I1ii11iIi11i + i11iIiiIii . OoOoOO00 / OoO0O00
  if 79 - 79: O0 * i11iIiiIii - IiII / IiII
  if 48 - 48: O0
  if 93 - 93: i11iIiiIii - I1IiiI * I1ii11iIi11i * I11i % O0 + OoooooooOO
  if 25 - 25: IiII + Ii1I / ooOoO0o . o0oOOo0O0Ooo % O0 * OoO0O00
  if 84 - 84: ooOoO0o % Ii1I + i11iIiiIii
  if 28 - 28: Oo0Ooo + OoO0O00 * OOooOOo % oO0o . I11i % O0
  if 16 - 16: I11i - iIii1I11I1II1 / I1IiiI . II111iiii + iIii1I11I1II1
  if 19 - 19: OoO0O00 - Oo0Ooo . O0
  if 60 - 60: II111iiii + Oo0Ooo
  if 9 - 9: ooOoO0o * OoooooooOO - iIii1I11I1II1 + OoOoOO00 / OoO0O00 . OoO0O00
  if 49 - 49: II111iiii
  if 25 - 25: OoooooooOO - I1IiiI . I1IiiI * oO0o
  if 81 - 81: iII111i + IiII
  if 98 - 98: I1IiiI
  if 95 - 95: ooOoO0o / ooOoO0o
  if 30 - 30: I1ii11iIi11i + Oo0Ooo / Oo0Ooo % I1ii11iIi11i . I1ii11iIi11i
  if 55 - 55: ooOoO0o - I11i + II111iiii + iII111i % Ii1I
  if 41 - 41: i1IIi - I11i - Ii1I
  if 8 - 8: OoO0O00 + I1Ii111 - o0oOOo0O0Ooo % Oo0Ooo % o0oOOo0O0Ooo * oO0o
  if 9 - 9: Oo0Ooo - i11iIiiIii - OOooOOo * Ii1I + ooOoO0o
  if 44 - 44: II111iiii
  if 52 - 52: I1ii11iIi11i - Oo0Ooo + I1ii11iIi11i % o0oOOo0O0Ooo
  if 35 - 35: iIii1I11I1II1
  if 42 - 42: I1Ii111 . I1IiiI . i1IIi + OoOoOO00 + OOooOOo + I1IiiI
  if 31 - 31: iII111i . OOooOOo - ooOoO0o . OoooooooOO / OoooooooOO
  if 56 - 56: OoO0O00 / oO0o / i11iIiiIii + OoooooooOO - Oo0Ooo - I11i
  if 21 - 21: O0 % IiII . I1IiiI / II111iiii + IiII
  if 53 - 53: oO0o - I1IiiI - oO0o * iII111i
 def XFSEMenuHandler ( self , inMenuRef , inItemRef ) :
  if 71 - 71: O0 - iIii1I11I1II1
  if ( inItemRef == 1 ) :
   if ( self . MenuItem1 == 1 ) :
    if 12 - 12: OOooOOo / o0oOOo0O0Ooo
    if 42 - 42: Oo0Ooo
    if 19 - 19: oO0o % I1ii11iIi11i * iIii1I11I1II1 + I1IiiI
    if ( not XPIsWidgetVisible ( self . XFSEWidget ) ) :
     XPShowWidget ( self . XFSEWidget )
  elif ( inItemRef == 2 ) :
   if ( self . MenuItem2 == 1 ) :
    if 46 - 46: Oo0Ooo
    if 1 - 1: iII111i
    if 97 - 97: OOooOOo + iII111i + O0 + i11iIiiIii
    if ( not XPIsWidgetVisible ( self . ACAliasWidget ) ) :
     XPShowWidget ( self . ACAliasWidget )
     if 77 - 77: o0oOOo0O0Ooo / OoooooooOO
     if 46 - 46: o0oOOo0O0Ooo % iIii1I11I1II1 . iII111i % iII111i + i11iIiiIii
     if 72 - 72: iIii1I11I1II1 * Ii1I % ooOoO0o / OoO0O00
 def XFSEpost ( self , query ) :
  I11i1II = open ( os . path . join ( 'Resources' , 'plugins' , 'PythonScripts' , 'PI_xfse.py' ) , 'rb' )
  Ooo = hashlib . md5 ( I11i1II . read ( ) ) . hexdigest ( )
  I11i1II . close ( )
  if 21 - 21: Oo0Ooo
  I1ii1 = 'http://www.fseconomy.net:81/fsagentx?md5sum=' + Ooo + '&' + query ;
  if 99 - 99: ooOoO0o . I1Ii111 % IiII * IiII . i1IIi
  try :
   O0OOoOOO0oO = urlopen ( I1ii1 ) . read ( )
  except IOError as I1ii11 :
   print "[XFSE|ERR] urlopen(): I/O error({0}): {1}" . format ( I1ii11 . errno , I1ii11 . strerror )
  except :
   print "[XFSE|ERR] urlopen(): general error when fetching data"
   if 74 - 74: Oo0Ooo - o0oOOo0O0Ooo . i1IIi
   if 43 - 43: iII111i / I1IiiI
   if 58 - 58: I1IiiI + i11iIiiIii % Ii1I . OoOoOO00
  if ( O0OOoOOO0oO . find ( "<?xml version=\"1.0\"?>" ) != 0 ) :
   print "[XFSE|ERR] = UNKNOWN SERVER RESPONSE ============"
   print "[XFSE|ERR] " + O0OOoOOO0oO
   print "[XFSE|ERR] ============ UNKNOWN SERVER RESPONSE ="
   O0OOoOOO0oO = ""
   if 13 - 13: i11iIiiIii + i1IIi * iIii1I11I1II1 % OoooooooOO - II111iiii * OOooOOo
  O0OOoOOO0oO = O0OOoOOO0oO . replace ( '&' , ' and ' )
  iiIi1iI1iIii = O0OOoOOO0oO
  O0OOoOOO0oO = ""
  for o00OooO0oo in iiIi1iI1iIii :
   o0o0oOoOO0O = ord ( o00OooO0oo )
   if o0o0oOoOO0O < 128 :
    O0OOoOOO0oO += chr ( o0o0oOoOO0O )
   else :
    O0OOoOOO0oO += '*'
    if 16 - 16: IiII % iIii1I11I1II1 . Ii1I
  oooooOOO000Oo = minidom . parseString ( O0OOoOOO0oO )
  return oooooOOO000Oo
  if 52 - 52: II111iiii % IiII . OoOoOO00 * iIii1I11I1II1
  if 50 - 50: ooOoO0o - I1Ii111 * IiII . I1ii11iIi11i
  if 37 - 37: ooOoO0o % i11iIiiIii % II111iiii . O0 . Ii1I
 def setInfoMessage ( self , msg1 , msg2 , msg3 , msg4 , color ) :
  self . errortext [ 0 ] = msg1
  self . errortext [ 1 ] = msg2
  self . errortext [ 2 ] = msg3
  self . errortext [ 3 ] = msg4
  self . errorcolor = color
  self . errormessage = 10
  XPSetWidgetDescriptor ( self . ErrorCaption [ 0 ] , self . errortext [ 0 ] )
  XPSetWidgetDescriptor ( self . ErrorCaption [ 1 ] , self . errortext [ 1 ] )
  XPSetWidgetDescriptor ( self . ErrorCaption [ 2 ] , self . errortext [ 2 ] )
  XPSetWidgetDescriptor ( self . ErrorCaption [ 3 ] , self . errortext [ 3 ] )
  if 51 - 51: OoO0O00 - O0 % oO0o - II111iiii
  if 31 - 31: iII111i / Oo0Ooo - iII111i - OOooOOo
  if 7 - 7: iII111i % O0 . OoOoOO00 + I1IiiI - I11i
 def isAllEngineStopped ( self ) :
  o0o0O00oo0 = True
  try :
   for Ii1ii1IiIII in range ( self . NumberOfEngines ) :
    if self . ACEngine [ Ii1ii1IiIII ] . isEngRun ( ) > 0 :
     o0o0O00oo0 = False
  except Exception :
   o0o0O00oo0 = True
   if 57 - 57: iIii1I11I1II1 / I11i - i1IIi
  return o0o0O00oo0
  if 51 - 51: IiII
 def setPlanePayload ( self , payload ) :
  XPLMSetDataf ( XPLMFindDataRef ( "sim/flightmodel/weight/m_fixed" ) , float ( payload ) )
  if 25 - 25: OoooooooOO + IiII * I1ii11iIi11i
  if 92 - 92: I1IiiI + I11i + O0 / o0oOOo0O0Ooo + I1Ii111
  if 18 - 18: ooOoO0o * OoOoOO00 . iII111i / I1ii11iIi11i / i11iIiiIii
 def setInstrGPS ( self , failmode ) :
  self . setInstrGPS_Vx ( failmode )
  if 21 - 21: oO0o / I1ii11iIi11i + Ii1I + OoooooooOO
  if 91 - 91: i11iIiiIii / i1IIi + iII111i + ooOoO0o * i11iIiiIii
  if 66 - 66: iIii1I11I1II1 % i1IIi - O0 + I11i * I1Ii111 . IiII
  if 52 - 52: ooOoO0o + O0 . iII111i . I1ii11iIi11i . OoO0O00
  if 97 - 97: I1IiiI / iII111i
 def setInstrAP ( self , failmode ) :
  if self . XPVer == 9 :
   self . setInstrAP_V9 ( failmode )
  else :
   self . setInstrAP_V10 ( failmode )
   if 71 - 71: II111iiii / i1IIi . I1ii11iIi11i % OoooooooOO . OoOoOO00
 def setInstrIFR ( self , failmode ) :
  if self . XPVer == 9 :
   self . setInstrIFR_V9 ( failmode )
  else :
   self . setInstrIFR_V10 ( failmode )
   if 41 - 41: i1IIi * II111iiii / OoooooooOO . OOooOOo
 def enableAllInstruments ( self ) :
  self . setInstrGPS ( 0 )
  self . setInstrAP ( 0 )
  self . setInstrIFR ( 0 )
  if 83 - 83: iII111i . O0 / Oo0Ooo / OOooOOo - II111iiii
  if 100 - 100: OoO0O00
  if 46 - 46: OoOoOO00 / iIii1I11I1II1 % iII111i . iIii1I11I1II1 * iII111i
 def setInstrGPS_Vx ( self , failmode ) :
  if ( failmode != 0 ) :
   IIi1ii1Ii = "sim/cockpit2/radios/actuators/HSI_source_select_pilot"
   if ( XPLMGetDatai ( XPLMFindDataRef ( IIi1ii1Ii ) ) == 2 ) :
    XPLMSetDatai ( XPLMFindDataRef ( IIi1ii1Ii ) , 0 )
   IIi1ii1Ii = "sim/cockpit2/radios/actuators/HSI_source_select_copilot"
   if ( XPLMGetDatai ( XPLMFindDataRef ( IIi1ii1Ii ) ) == 2 ) :
    XPLMSetDatai ( XPLMFindDataRef ( IIi1ii1Ii ) , 0 )
   IIi1ii1Ii = "sim/cockpit2/radios/actuators/RMI_source_select_pilot"
   if ( XPLMGetDatai ( XPLMFindDataRef ( IIi1ii1Ii ) ) == 2 ) :
    XPLMSetDatai ( XPLMFindDataRef ( IIi1ii1Ii ) , 0 )
   IIi1ii1Ii = "sim/cockpit2/radios/actuators/RMI_source_select_copilot"
   if ( XPLMGetDatai ( XPLMFindDataRef ( IIi1ii1Ii ) ) == 2 ) :
    XPLMSetDatai ( XPLMFindDataRef ( IIi1ii1Ii ) , 0 )
    if 91 - 91: i11iIiiIii / OoooooooOO + iII111i - i11iIiiIii + OOooOOo
    if 18 - 18: II111iiii / IiII
    if 4 - 4: II111iiii / I1ii11iIi11i + II111iiii . iIii1I11I1II1
    if 20 - 20: oO0o - Ii1I % Ii1I * o0oOOo0O0Ooo + I1Ii111 + o0oOOo0O0Ooo
    if 39 - 39: i1IIi / IiII
    if 78 - 78: ooOoO0o
    if 53 - 53: ooOoO0o * OOooOOo . iII111i / O0 * ooOoO0o
 def setInstrAP_V10 ( self , failmode ) :
  XPLMSetDatai ( XPLMFindDataRef ( "sim/operation/failures/rel_auto_servos" ) , int ( failmode ) )
  XPLMSetDatai ( XPLMFindDataRef ( "sim/operation/failures/rel_otto" ) , int ( failmode ) )
  XPLMSetDatai ( XPLMFindDataRef ( "sim/operation/failures/rel_servo_ailn" ) , int ( failmode ) )
  XPLMSetDatai ( XPLMFindDataRef ( "sim/operation/failures/rel_servo_elev" ) , int ( failmode ) )
  XPLMSetDatai ( XPLMFindDataRef ( "sim/operation/failures/rel_servo_rudd" ) , int ( failmode ) )
  XPLMSetDatai ( XPLMFindDataRef ( "sim/operation/failures/rel_servo_thro" ) , int ( failmode ) )
  if 22 - 22: Oo0Ooo % iII111i * I1ii11iIi11i / OOooOOo % i11iIiiIii * I11i
 def setInstrIFR_V10 ( self , failmode ) :
  if 95 - 95: OoooooooOO - IiII * I1IiiI + OoOoOO00
  if 10 - 10: o0oOOo0O0Ooo / i11iIiiIii
  if 92 - 92: I11i . I1Ii111
  XPLMSetDatai ( XPLMFindDataRef ( "sim/operation/failures/rel_gls" ) , int ( failmode ) )
  XPLMSetDatai ( XPLMFindDataRef ( "sim/operation/failures/rel_dme" ) , int ( failmode ) )
  XPLMSetDatai ( XPLMFindDataRef ( "sim/operation/failures/rel_adf1" ) , int ( failmode ) )
  XPLMSetDatai ( XPLMFindDataRef ( "sim/operation/failures/rel_adf2" ) , int ( failmode ) )
  XPLMSetDatai ( XPLMFindDataRef ( "sim/operation/failures/rel_nav1" ) , int ( failmode ) )
  XPLMSetDatai ( XPLMFindDataRef ( "sim/operation/failures/rel_nav2" ) , int ( failmode ) )
  if 85 - 85: I1ii11iIi11i . I1Ii111
  if 78 - 78: ooOoO0o * I1Ii111 + iIii1I11I1II1 + iIii1I11I1II1 / I1Ii111 . Ii1I
  if 97 - 97: ooOoO0o / I1Ii111 % i1IIi % I1ii11iIi11i
  if 18 - 18: iIii1I11I1II1 % I11i
  if 95 - 95: ooOoO0o + i11iIiiIii * I1Ii111 - i1IIi * I1Ii111 - iIii1I11I1II1
  if 75 - 75: OoooooooOO * IiII
  if 9 - 9: IiII - II111iiii + O0 / iIii1I11I1II1 / i11iIiiIii
  if 39 - 39: IiII * Oo0Ooo + iIii1I11I1II1 - IiII + OOooOOo
  if 69 - 69: O0
  if 85 - 85: ooOoO0o / O0
  if 18 - 18: o0oOOo0O0Ooo % O0 * I1ii11iIi11i
  if 62 - 62: I1Ii111 . IiII . OoooooooOO
  if 11 - 11: OOooOOo / I11i
  if 73 - 73: i1IIi / i11iIiiIii
  if 58 - 58: Oo0Ooo . II111iiii + oO0o - i11iIiiIii / II111iiii / O0
  if 85 - 85: OoOoOO00 + OOooOOo
  if 10 - 10: IiII / OoO0O00 + OoOoOO00 / i1IIi
  if 27 - 27: Ii1I
  if 67 - 67: I1IiiI
  if 55 - 55: I1ii11iIi11i - iII111i * o0oOOo0O0Ooo + OoOoOO00 * OoOoOO00 * O0
  if 91 - 91: I1Ii111 - OOooOOo % iIii1I11I1II1 - OoooooooOO % ooOoO0o
  if 98 - 98: OoO0O00 . OoO0O00 * oO0o * II111iiii * I1Ii111
  if 92 - 92: Oo0Ooo
  if 40 - 40: OoOoOO00 / IiII
  if 79 - 79: OoO0O00 - iIii1I11I1II1 + Ii1I - I1Ii111
  if 93 - 93: II111iiii . I1IiiI - Oo0Ooo + OoOoOO00
 def setInstrAP_V9 ( self , failmode ) :
  XPLMSetDatai ( XPLMFindDataRef ( "sim/operation/failures/rel_otto" ) , int ( failmode ) )
  if 61 - 61: II111iiii
 def setInstrIFR_V9 ( self , failmode ) :
  XPLMSetDatai ( XPLMFindDataRef ( "sim/operation/failures/rel_g_gs1" ) , int ( failmode ) )
  XPLMSetDatai ( XPLMFindDataRef ( "sim/operation/failures/rel_g_gs2" ) , int ( failmode ) )
  XPLMSetDatai ( XPLMFindDataRef ( "sim/operation/failures/rel_g_navrad1" ) , int ( failmode ) )
  XPLMSetDatai ( XPLMFindDataRef ( "sim/operation/failures/rel_g_navrad2" ) , int ( failmode ) )
  XPLMSetDatai ( XPLMFindDataRef ( "sim/operation/failures/rel_gls" ) , int ( failmode ) )
  XPLMSetDatai ( XPLMFindDataRef ( "sim/operation/failures/rel_dme" ) , int ( failmode ) )
  XPLMSetDatai ( XPLMFindDataRef ( "sim/operation/failures/rel_adf1" ) , int ( failmode ) )
  XPLMSetDatai ( XPLMFindDataRef ( "sim/operation/failures/rel_adf2" ) , int ( failmode ) )
  XPLMSetDatai ( XPLMFindDataRef ( "sim/operation/failures/rel_nav1" ) , int ( failmode ) )
  XPLMSetDatai ( XPLMFindDataRef ( "sim/operation/failures/rel_nav2" ) , int ( failmode ) )
  if 15 - 15: i11iIiiIii % I1IiiI * I11i / I1Ii111
  if 90 - 90: iII111i
  if 31 - 31: OOooOOo + O0
  if 87 - 87: ooOoO0o
  if 45 - 45: OoO0O00 / OoooooooOO - iII111i / Ii1I % IiII
  if 83 - 83: I1IiiI . iIii1I11I1II1 - IiII * i11iIiiIii
  if 20 - 20: i1IIi * I1Ii111 + II111iiii % o0oOOo0O0Ooo % oO0o
  if 13 - 13: Oo0Ooo
  if 60 - 60: I1ii11iIi11i * I1IiiI
  if 17 - 17: OOooOOo % Oo0Ooo / I1ii11iIi11i . IiII * OOooOOo - II111iiii
  if 41 - 41: Ii1I
  if 77 - 77: I1Ii111
  if 65 - 65: II111iiii . I1IiiI % oO0o * OoO0O00
  if 38 - 38: OoOoOO00 / iII111i % Oo0Ooo
  if 11 - 11: iII111i - oO0o + II111iiii - iIii1I11I1II1
  if 7 - 7: IiII - I11i / II111iiii * Ii1I . iII111i * iII111i
  if 61 - 61: I11i % ooOoO0o - OoO0O00 / Oo0Ooo
  if 4 - 4: OoooooooOO - i1IIi % Ii1I - OOooOOo * o0oOOo0O0Ooo
  if 85 - 85: OoooooooOO * iIii1I11I1II1 . iII111i / OoooooooOO % I1IiiI % O0
 def checkACState ( self , elapsedMe , elapsedSim , counter , refcon ) :
  if ( self . errormessage > 0 ) :
   self . errormessage = self . errormessage - 1
   if 36 - 36: Ii1I / II111iiii / IiII / IiII + I1ii11iIi11i
  oO0Ooo0ooOO0 = XPLMGetDatai ( XPLMFindDataRef ( "sim/time/ground_speed" ) )
  XPLMSetDatai ( XPLMFindDataRef ( "sim/time/ground_speed" ) , 1 )
  if 46 - 46: Ii1I % OoOoOO00
  if 64 - 64: i11iIiiIii - II111iiii
  self . flightTimer = XPLMGetDataf ( XPLMFindDataRef ( "sim/time/total_flight_time_sec" ) )
  if 77 - 77: OoOoOO00 % Ii1I
  if self . flying == 1 :
   if 9 - 9: OoO0O00 - Oo0Ooo * OoooooooOO . Oo0Ooo
   if oO0Ooo0ooOO0 > 1 :
    self . gsCheat += 1
    if 2 - 2: OoooooooOO % OOooOOo
   if self . gsCheat > 10 :
    self . cancelFlight ( "Excessive time compression used. Your flight has been cancelled" )
    if 63 - 63: I1IiiI % iIii1I11I1II1
   if self . ACEngine [ 0 ] . engineType ( ) == 3 or self . ACEngine [ 0 ] . engineType ( ) == 5 :
    I1ii = 1
   else :
    I1ii = 0
    if 73 - 73: IiII + I1IiiI * Oo0Ooo * OoooooooOO
   Oo0o0O = XPLMGetDataf ( XPLMFindDataRef ( "sim/flightmodel/position/groundspeed" ) )
   if 17 - 17: i11iIiiIii - II111iiii * o0oOOo0O0Ooo
   if 5 - 5: OOooOOo - OOooOOo . Oo0Ooo + OoOoOO00 - OOooOOo . oO0o
   IiIi1i1ii = XPLMGetDataf ( XPLMFindDataRef ( "sim/flightmodel/weight/m_fuel_total" ) )
   if 11 - 11: II111iiii / o0oOOo0O0Ooo
   if 21 - 21: i11iIiiIii / i1IIi + I1IiiI * OOooOOo . I1Ii111
   if ( ( int ( IiIi1i1ii ) * 0.95 ) > int ( self . checkfuel ) ) :
    self . cancelFlight ( "Airborn refueling not allowed. Flight cancelled" , "" )
    if 84 - 84: O0 . I11i - II111iiii . ooOoO0o / II111iiii
   self . checkfuel = XPLMGetDataf ( XPLMFindDataRef ( "sim/flightmodel/weight/m_fuel_total" ) )
   if 47 - 47: OoooooooOO
   if 4 - 4: I1IiiI % I11i
   if ( self . flightTimer < self . flightTimerLast ) :
    self . cancelFlight ( "Aircraft changed or repositioned. Your flight has been cancelled" , "" )
   self . flightTimerLast = self . flightTimer
   if 10 - 10: IiII . OoooooooOO - OoO0O00 + IiII - O0
   if 82 - 82: ooOoO0o + II111iiii
   self . flightTime = int ( self . flightTimer - self . flightStart )
   if 39 - 39: oO0o % iIii1I11I1II1 % O0 % OoooooooOO * I1ii11iIi11i + iII111i
   if 68 - 68: Oo0Ooo + i11iIiiIii
   if self . leaseTime > 0 :
    self . leaseTime = int ( self . leaseStart - self . flightTime )
    if 69 - 69: iIii1I11I1II1 * iIii1I11I1II1 * i11iIiiIii + I1IiiI / OOooOOo % Ii1I
    if 58 - 58: OOooOOo * o0oOOo0O0Ooo + O0 % OOooOOo
   if self . LeaseCaption :
    iI1I1iIi11 = ""
    if 87 - 87: OoOoOO00
    IioO0O = self . flightTime / 3600
    oOO = ( self . flightTime - IioO0O * 3600 ) / 60
    iI1I1iIi11 += "Current flight time: " + str ( IioO0O ) + " hours " + str ( oOO ) + " mins"
    if 11 - 11: i11iIiiIii - oO0o . oO0o
    I11I = self . leaseTime / 3600
    iIIII1i = ( self . leaseTime - I11I * 3600 ) / 60
    iI1I1iIi11 += " - Lease time left: " + str ( I11I ) + " hours " + str ( iIIII1i ) + " mins"
    if 76 - 76: iII111i + ooOoO0o
    XPSetWidgetDescriptor ( self . LeaseCaption , iI1I1iIi11 )
    if 30 - 30: i11iIiiIii % iIii1I11I1II1 . I11i % iIii1I11I1II1
    if 62 - 62: Oo0Ooo * OoOoOO00
   if self . EndFlightCaption :
    if ( self . airborne == 0 ) :
     iI1I1iIi11 = "To go airborne:"
     if ( self . ACEngine [ 0 ] . currentRPM ( ) < float ( self . startPlaneRpm ) ) :
      iI1I1iIi11 += " RPM>" + str ( self . startPlaneRpm ) + "/min"
      if 79 - 79: OoO0O00 . iII111i * Ii1I - OOooOOo + ooOoO0o
     if ( self . ACEngine [ 0 ] . planeALT ( ) < self . startPlaneAlt ) :
      iI1I1iIi11 += " ALT>" + str ( self . startPlaneAlt ) + "ft"
      if 14 - 14: i11iIiiIii - iII111i * OoOoOO00
     if ( Oo0o0O < self . startPlaneSpd ) :
      iI1I1iIi11 += " SPD>" + str ( self . startPlaneSpd ) + "kts"
      if 51 - 51: I1ii11iIi11i / iIii1I11I1II1 % oO0o + o0oOOo0O0Ooo * ooOoO0o + I1Ii111
    else :
     iI1I1iIi11 = ""
     if ( self . flightTime < self . endFlightTime ) :
      iI1I1iIi11 += " FltTim>" + str ( self . endFlightTime ) + "s"
      if 77 - 77: ooOoO0o * OoOoOO00
     if ( self . ACEngine [ 0 ] . planeALT ( ) > self . endPlaneAlt ) :
      iI1I1iIi11 += " ALT<" + str ( self . endPlaneAlt ) + "ft"
      if 14 - 14: I11i % I11i / IiII
     if ( Oo0o0O > self . endPlaneSpd ) :
      iI1I1iIi11 += " SPD<" + str ( self . endPlaneSpd ) + "kts"
      if 72 - 72: i1IIi - II111iiii - OOooOOo + OOooOOo * o0oOOo0O0Ooo * OOooOOo
     for Ii1ii1IiIII in range ( self . NumberOfEngines ) :
      if self . ACEngine [ Ii1ii1IiIII ] . isEngRun ( ) > 0 :
       iI1I1iIi11 += " Eng" + str ( Ii1ii1IiIII + 1 )
     if ( len ( iI1I1iIi11 ) == 0 ) :
      iI1I1iIi11 = " press the 'Finish Flight' button to log the flight to the server!"
      if 33 - 33: Oo0Ooo
     iI1I1iIi11 = "To end flight:" + iI1I1iIi11
     if 49 - 49: OoO0O00 % iII111i % iII111i / iII111i
     if 53 - 53: iIii1I11I1II1
    XPSetWidgetDescriptor ( self . EndFlightCaption , iI1I1iIi11 )
    if 68 - 68: OoooooooOO % II111iiii
    if 26 - 26: II111iiii % i11iIiiIii % iIii1I11I1II1 % I11i * I11i * I1ii11iIi11i
   if ( self . ACEngine [ 0 ] . currentRPM ( ) > float ( self . startPlaneRpm ) and Oo0o0O > float ( self . startPlaneSpd ) and self . ACEngine [ 0 ] . planeALT ( ) > self . startPlaneAlt ) :
    self . airborne = 1
    self . Transmitting = 0
    if 24 - 24: II111iiii % I1Ii111 - ooOoO0o + I1IiiI * I1ii11iIi11i
    for i11111I1I in range ( self . NumberOfEngines ) :
     if 61 - 61: II111iiii * OOooOOo / OoooooooOO / Oo0Ooo - OoO0O00
     self . ACEngine [ i11111I1I ] . feed ( 1 , self . ACEngine [ i11111I1I ] . currentRPM ( ) , self . ACEngine [ i11111I1I ] . currentMIX ( ) , self . ACEngine [ i11111I1I ] . currentCHT ( ) , self . ACEngine [ i11111I1I ] . planeALT ( ) )
     if 56 - 56: I1ii11iIi11i
     if 26 - 26: OoooooooOO % OoooooooOO
   else :
    if ( self . airborne >= 1 ) :
     if ( self . flightTime > self . endFlightTime and self . isAllEngineStopped ( ) and self . ACEngine [ 0 ] . planeALT ( ) < self . endPlaneAlt and Oo0o0O < float ( self . endPlaneSpd ) ) :
      XPSetWidgetProperty ( self . EndFlyButton , xpProperty_Enabled , 1 )
      self . airborne = 2
     else :
      XPSetWidgetProperty ( self . EndFlyButton , xpProperty_Enabled , 0 )
      self . airborne = 1
      if 33 - 33: I1Ii111
      if 62 - 62: I1ii11iIi11i + Ii1I + i1IIi / OoooooooOO
   if ( self . stEq == "0" ) :
    self . setInstrAP ( 6 )
    self . setInstrGPS ( 6 )
    self . setInstrIFR ( 6 )
    if 7 - 7: o0oOOo0O0Ooo + i1IIi . I1IiiI / Oo0Ooo
   if ( self . stEq == "1" ) :
    self . setInstrAP ( 6 )
    self . setInstrGPS ( 6 )
    if 22 - 22: ooOoO0o - ooOoO0o % OOooOOo . I1Ii111 + oO0o
   if ( self . stEq == "2" ) :
    self . setInstrAP ( 6 )
    self . setInstrIFR ( 6 )
    if 63 - 63: I1IiiI % I1Ii111 * o0oOOo0O0Ooo + I1Ii111 / Oo0Ooo % iII111i
   if ( self . stEq == "4" ) :
    self . setInstrGPS ( 6 )
    self . setInstrIFR ( 6 )
    if 45 - 45: IiII
   if ( self . stEq == "3" ) :
    self . setInstrAP ( 6 )
    if 20 - 20: OoooooooOO * o0oOOo0O0Ooo * O0 . OOooOOo
   if ( self . stEq == "5" ) :
    self . setInstrGPS ( 6 )
    if 78 - 78: iIii1I11I1II1 + I11i - Ii1I * I1Ii111 - OoooooooOO % OoOoOO00
   if ( self . stEq == "6" ) :
    self . setInstrIFR ( 6 )
    if 34 - 34: O0
    if 80 - 80: i1IIi - Oo0Ooo / OoO0O00 - i11iIiiIii
    if 68 - 68: oO0o - I1ii11iIi11i % O0 % I1Ii111
    if 11 - 11: O0 / OoO0O00 % OOooOOo + o0oOOo0O0Ooo + iIii1I11I1II1
    if 40 - 40: ooOoO0o - OOooOOo . Ii1I * Oo0Ooo % I1Ii111
    if 56 - 56: i11iIiiIii . o0oOOo0O0Ooo - I1IiiI * I11i
    if 91 - 91: oO0o + OoooooooOO - i1IIi
    if 84 - 84: Ii1I / IiII
    if 86 - 86: OoOoOO00 * II111iiii - O0 . OoOoOO00 % iIii1I11I1II1 / OOooOOo
  return float ( 1 )
  if 11 - 11: I1IiiI * oO0o + I1ii11iIi11i / I1ii11iIi11i
  if 37 - 37: i11iIiiIii + i1IIi
  if 23 - 23: iII111i + I11i . OoOoOO00 * I1IiiI + I1ii11iIi11i
 def startFly ( self ) :
  if ( self . flying == 1 ) :
   print "[XFSE|WRN] Start flight function is disabled"
  else :
   print "[XFSE|dbg] Start flight function"
   I1iIi1iiiIiI = open ( os . path . join ( 'Resources' , 'plugins' , 'PythonScripts' , 'x-economy.ini' ) , 'w' )
   I1iIi1iiiIiI . write ( self . userstr + '\n' + self . passstr )
   I1iIi1iiiIiI . close ( )
   if 41 - 41: I1ii11iIi11i * ooOoO0o - Ii1I + Oo0Ooo
   self . cancelCmdFlag = 0
   self . cancelBtnFlag = 0
   if 23 - 23: II111iiii % o0oOOo0O0Ooo + o0oOOo0O0Ooo + iII111i - iII111i
   if 62 - 62: o0oOOo0O0Ooo
   self . CurrentAircraft = self . ReadACAliasFromFile ( )
   if 45 - 45: OOooOOo * ooOoO0o
   if ( self . CurrentAircraft == "" ) :
    ooOoOo = [ ]
    XPLMGetDatab ( XPLMFindDataRef ( "sim/aircraft/view/acf_descrip" ) , ooOoOo , 0 , 500 )
    self . CurrentAircraft = ooOoOo [ 0 ] . replace ( ' ' , '%20' )
    if 21 - 21: OoOoOO00 + i11iIiiIii + I1IiiI * o0oOOo0O0Ooo % iII111i % II111iiii
   print "[XFSE|Nfo] Current AC: " + self . CurrentAircraft
   if 55 - 55: Oo0Ooo - OOooOOo
   if 84 - 84: I1Ii111 + Oo0Ooo - OoOoOO00 * OoOoOO00
   self . ACEngine = [ ]
   if 61 - 61: OoooooooOO . oO0o . OoooooooOO / Oo0Ooo
   if 72 - 72: i1IIi
   self . NumberOfEngines = int ( XPLMGetDatai ( XPLMFindDataRef ( "sim/aircraft/engine/acf_num_engines" ) ) )
   print "[XFSE|Nfo] Number of engines: " + str ( self . NumberOfEngines )
   OOoo0oo = XPLMGetDataf ( XPLMFindDataRef ( "sim/weather/temperature_ambient_c" ) )
   if 58 - 58: oO0o
   for IiIIi1IiiI1 in range ( self . NumberOfEngines ) :
    self . ACEngine . append ( engine ( OOoo0oo , 0 , 0 , 0 , IiIIi1IiiI1 ) )
    if 87 - 87: oO0o % Ii1I
    if 83 - 83: II111iiii - I11i
   for iiIii1IIi in range ( len ( self . FromCaption ) ) :
    XPDestroyWidget ( self , self . FromCaption [ iiIii1IIi ] , 1 )
    XPDestroyWidget ( self , self . ToCaption [ iiIii1IIi ] , 1 )
    XPDestroyWidget ( self , self . CargoCaption [ iiIii1IIi ] , 1 )
    if 10 - 10: i11iIiiIii - o0oOOo0O0Ooo % iIii1I11I1II1
   self . FromCaption = [ ]
   self . ToCaption = [ ]
   self . CargoCaption = [ ]
   if 49 - 49: oO0o
   if 83 - 83: oO0o % Oo0Ooo - o0oOOo0O0Ooo . iII111i / Oo0Ooo % I1ii11iIi11i
   if 75 - 75: O0 % OoOoOO00 . IiII / IiII / OoO0O00
   if 19 - 19: I1ii11iIi11i % i11iIiiIii . OOooOOo - Oo0Ooo / OoooooooOO
   if 66 - 66: OoO0O00 * Oo0Ooo
   if 28 - 28: OoO0O00 % OoOoOO00 % I1ii11iIi11i + I1IiiI / I1IiiI
   if 71 - 71: OOooOOo * OoO0O00 % OoooooooOO % OoO0O00 / I1IiiI
   if 56 - 56: OoooooooOO % i11iIiiIii * iIii1I11I1II1 . OoO0O00 * O0
   iI = XPLMFindDataRef ( "sim/flightmodel/position/latitude" )
   O0O00OOo = XPLMFindDataRef ( "sim/flightmodel/position/longitude" )
   OoOOo = XPLMGetDataf ( iI )
   iii1 = XPLMGetDataf ( O0O00OOo )
   if 78 - 78: I1ii11iIi11i + I11i - O0
   i1I1iIi1IiI = self . XFSEpost ( "user=" + self . userstr + "&pass=" + self . passstr + "&action=startFlight&lat=" + str ( OoOOo ) + "&lon=" + str ( iii1 ) + "&aircraft=" + self . CurrentAircraft . replace ( ' ' , '%20' ) )
   if 11 - 11: II111iiii
   if i1I1iIi1IiI . getElementsByTagName ( 'response' ) [ 0 ] . firstChild . nodeName == "error" :
    O00O00O000OOO = i1I1iIi1IiI . getElementsByTagName ( 'error' ) [ 0 ] . firstChild . data
    iIOo0O = O00O00O000OOO . find ( "is not compatible with your rented" )
    if 1 - 1: O0 / iII111i % I1Ii111 . Oo0Ooo + IiII
    if iIOo0O > 0 :
     self . setInfoMessage ( "Your flight has not been started: Aircraft alias does not match!" ,
 "FSE=[" + O00O00O000OOO [ iIOo0O + 35 : ] + "] X-Plane=[" + O00O00O000OOO [ : iIOo0O - 1 ] + "]" ,
 "Pick an aircraft alias from the FSE website 'Home->Aircraft models .. Request aliases' and enter it to" ,
 "the 'Custom Alias' window ('Plugins Menu->X-Economy->Set Aircraft alias') ... or ask the forum for help!" ,
 "red" )
     if ( not XPIsWidgetVisible ( self . ACAliasWidget ) ) :
      XPShowWidget ( self . ACAliasWidget )
    else :
     self . setInfoMessage ( "Your flight has not been started:" ,
 i1I1iIi1IiI . getElementsByTagName ( 'error' ) [ 0 ] . firstChild . data ,
 "" ,
 "" ,
 "red" )
     if 27 - 27: I1Ii111 % OoooooooOO + IiII % i1IIi / oO0o / OoooooooOO
     if 11 - 11: OOooOOo % Ii1I - i11iIiiIii - oO0o + ooOoO0o + IiII
   else :
    if 87 - 87: I1Ii111 * i1IIi / I1ii11iIi11i
    IIII1i1 = "-"
    oooOoOoOoO = "-"
    oo0O0oo = "-"
    Iiiii1i = 14
    if 81 - 81: ooOoO0o + iII111i
    OoOOO0o0OOO = len ( i1I1iIi1IiI . getElementsByTagName ( 'assignment' ) )
    for i1iii11i in range ( OoOOO0o0OOO ) :
     if i1iii11i < Iiiii1i :
      self . addAssignment ( i1iii11i , str ( i1I1iIi1IiI . getElementsByTagName ( 'from' ) [ i1iii11i ] . firstChild . data ) , str ( i1I1iIi1IiI . getElementsByTagName ( 'to' ) [ i1iii11i ] . firstChild . data ) , str ( i1I1iIi1IiI . getElementsByTagName ( 'cargo' ) [ i1iii11i ] . firstChild . data ) )
     if i1iii11i == Iiiii1i :
      if OoOOO0o0OOO == Iiiii1i + 1 :
       self . addAssignment ( i1iii11i , str ( i1I1iIi1IiI . getElementsByTagName ( 'from' ) [ i1iii11i ] . firstChild . data ) , str ( i1I1iIi1IiI . getElementsByTagName ( 'to' ) [ i1iii11i ] . firstChild . data ) , str ( i1I1iIi1IiI . getElementsByTagName ( 'cargo' ) [ i1iii11i ] . firstChild . data ) )
      else :
       self . addAssignment ( i1iii11i , "[...]" , "[...]" , str ( ( OoOOO0o0OOO - Iiiii1i ) ) + " additional assignments" )
       if 58 - 58: Ii1I / OOooOOo + I11i + IiII
    iii1III1i = i1I1iIi1IiI . getElementsByTagName ( 'accounting' ) [ 0 ] . firstChild . data
    if 17 - 17: II111iiii / II111iiii
    self . stEq = i1I1iIi1IiI . getElementsByTagName ( 'equipment' ) [ 0 ] . firstChild . data
    if 65 - 65: IiII + Oo0Ooo
    if ( self . stEq == "0" ) :
     Ooo0O0 = " (VFR)"
    if ( self . stEq == "1" ) :
     Ooo0O0 = " (IFR)"
    if ( self . stEq == "2" ) :
     Ooo0O0 = " (GPS)"
    if ( self . stEq == "4" ) :
     Ooo0O0 = " (AP)"
    if ( self . stEq == "3" ) :
     Ooo0O0 = " (IFR, GPS)"
    if ( self . stEq == "5" ) :
     Ooo0O0 = " (AP, IFR)"
    if ( self . stEq == "6" ) :
     Ooo0O0 = " (AP, GPS)"
    if ( self . stEq == "7" ) :
     Ooo0O0 = " (IFR, AP, GPS)"
     if 71 - 71: OoooooooOO
    iIiI1iiiI1ii = i1I1iIi1IiI . getElementsByTagName ( 'registration' ) [ 0 ] . firstChild . data
    I1i = i1I1iIi1IiI . getElementsByTagName ( 'leaseExpires' ) [ 0 ] . firstChild . data
    XPSetWidgetDescriptor ( self . ACRegCaption , "Aircraft registration: " + str ( iIiI1iiiI1ii ) + str ( Ooo0O0 ) )
    if 67 - 67: OoOoOO00 / o0oOOo0O0Ooo * OoO0O00 / OOooOOo * I1ii11iIi11i / oO0o
    self . leaseTime = 0
    self . leaseTime = int ( I1i )
    self . leaseStart = int ( I1i )
    if 64 - 64: oO0o - I1IiiI / iII111i - OoO0O00
    if 37 - 37: i11iIiiIii / iII111i
    self . stPayload = i1I1iIi1IiI . getElementsByTagName ( 'payloadWeight' ) [ 0 ] . firstChild . data
    self . setPlanePayload ( self . stPayload )
    if 85 - 85: i11iIiiIii + I1Ii111 * OoOoOO00
    if 1 - 1: i1IIi / Oo0Ooo . OoO0O00
    o0O = i1I1iIi1IiI . getElementsByTagName ( 'fuel' ) [ 0 ] . firstChild . data
    ii1111i = o0O . split ( ' ' )
    if 75 - 75: IiII + II111iiii / oO0o - oO0o / OoooooooOO
    if 2 - 2: o0oOOo0O0Ooo
    self . FuelTanks = [ ]
    i11ii = float ( 0 )
    for i11I1 in range ( len ( ii1111i ) - 1 ) :
     i11ii += float ( ii1111i [ i11I1 ] )
     if float ( ii1111i [ i11I1 ] ) > float ( 0 ) :
      self . FuelTanks . append ( 1 )
     else :
      self . FuelTanks . append ( 0 )
      if 34 - 34: O0 * O0 % OoooooooOO + iII111i * iIii1I11I1II1 % Ii1I
    I1iI1I1 = 9
    if 48 - 48: I1IiiI / i11iIiiIii - o0oOOo0O0Ooo * oO0o / OoooooooOO
    OoOo = i11ii * self . constKgToGal
    if 17 - 17: Ii1I . i11iIiiIii
    if 5 - 5: I1ii11iIi11i + O0 + O0 . I1Ii111 - ooOoO0o
    o00oo0000 = XPLMGetDataf ( XPLMFindDataRef ( "sim/aircraft/weight/acf_m_fuel_tot" ) )
    print "[XFSE|Nfo] --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---"
    print "[XFSE|Nfo] FSE Fuel for Plane       : " + str ( int ( OoOo / self . constKgToGal ) ) + "gal"
    print "[XFSE|Nfo] --- --- --- --- --- --- ---"
    print "[XFSE|Nfo] Aircraft Model Total Fuel: " + str ( int ( o00oo0000 / self . constKgToGal ) ) + "gal"
    if 44 - 44: Oo0Ooo % iIii1I11I1II1
    print "[XFSE|Nfo] Aircraft Model Fuel Tanks: "
    oo0ooO0 = [ ]
    XPLMGetDatavf ( XPLMFindDataRef ( "sim/aircraft/overflow/acf_tank_rat" ) , oo0ooO0 , 0 , I1iI1I1 )
    if 28 - 28: I1ii11iIi11i * OoooooooOO . II111iiii / i11iIiiIii + oO0o
    for i1 in range ( I1iI1I1 ) :
     if ( oo0ooO0 [ i1 ] > 0 ) :
      print "[XFSE|Nfo] ... Tank #" + str ( i1 ) + ": " + str ( int ( oo0ooO0 [ i1 ] * 100 ) ) + "% - " + str ( int ( oo0ooO0 [ i1 ] * o00oo0000 / self . constKgToGal ) ) + "gal"
      if 78 - 78: OOooOOo % o0oOOo0O0Ooo
      if 39 - 39: I1ii11iIi11i + I1IiiI - iIii1I11I1II1 - o0oOOo0O0Ooo
    I1ii1II1iII = [ 0.0 , 0.0 , 0.0 , 0.0 , 0.0 , 0.0 , 0.0 , 0.0 , 0.0 ]
    II1i = OoOo
    if 81 - 81: I1ii11iIi11i
    print "[XFSE|Nfo] --- --- --- --- --- --- ---"
    if 94 - 94: I11i + II111iiii % i11iIiiIii
    if 8 - 8: ooOoO0o * O0
    OOoOIiIIII = self . ReadFuelTankConfFile ( )
    if ( len ( OOoOIiIIII ) == 0 ) :
     print "[XFSE|Nfo] Aircraft Fuel Distribution: no config - using DEFAULTs"
     if 89 - 89: OoO0O00 / I1IiiI
     if ( OoOo > o00oo0000 ) :
      IIi1iI1 = o00oo0000
      II1i = OoOo - o00oo0000
     else :
      IIi1iI1 = OoOo
      II1i = 0
      if 44 - 44: I1ii11iIi11i - Ii1I / II111iiii * OoO0O00 * Oo0Ooo
     for i1 in range ( I1iI1I1 ) :
      I1ii1II1iII [ i1 ] = ( IIi1iI1 * oo0ooO0 [ i1 ] )
      if ( oo0ooO0 [ i1 ] > 0 ) :
       print "[XFSE|Nfo] ... Fill Tank #" + str ( i1 ) + " to " + str ( int ( I1ii1II1iII [ i1 ] / self . constKgToGal ) ) + "gal"
       if 73 - 73: o0oOOo0O0Ooo - I1IiiI * i1IIi / i11iIiiIii * OOooOOo % II111iiii
    else :
     print "[XFSE|Nfo] Aircraft Fuel Distribution: xfse_fueltanks.txt"
     for i1 in range ( len ( OOoOIiIIII ) ) :
      if 56 - 56: OoooooooOO * Oo0Ooo . Oo0Ooo . I1ii11iIi11i
      if ( len ( OOoOIiIIII [ i1 ] ) == 1 ) :
       II1 = int ( OOoOIiIIII [ i1 ] [ 0 ] )
       if ( II1 >= 0 and II1 <= 8 ) :
        Oo00O0Oo0Oo = o00oo0000 * oo0ooO0 [ II1 ]
        if ( II1i > Oo00O0Oo0Oo ) :
         II1i -= Oo00O0Oo0Oo
         I1ii1II1iII [ II1 ] = float ( Oo00O0Oo0Oo )
        else :
         I1ii1II1iII [ II1 ] = float ( II1i )
         II1i = 0
        print "[XFSE|Nfo] ... Prio #" + str ( i1 ) + ": Fill Tank #" + str ( II1 ) + " to " + str ( int ( I1ii1II1iII [ II1 ] / self . constKgToGal ) ) + " (of " + str ( int ( Oo00O0Oo0Oo / self . constKgToGal ) ) + ") gal"
       else :
        print "[XFSE|ERR] ... Error reading fuel tank config line!"
        if 6 - 6: iII111i + IiII + i1IIi * oO0o - i11iIiiIii
        if 78 - 78: i11iIiiIii / iIii1I11I1II1 - o0oOOo0O0Ooo
      if ( len ( OOoOIiIIII [ i1 ] ) == 3 ) :
       II1 = OOoOIiIIII [ i1 ]
       iIIIIiiIii = int ( II1 [ 0 ] )
       ooO0oo = int ( II1 [ 2 ] )
       if ( iIIIIiiIii >= 0 and iIIIIiiIii <= 8 and ooO0oo >= 0 and ooO0oo <= 8 ) :
        ooO0oo0o0 = ( o00oo0000 * oo0ooO0 [ iIIIIiiIii ] )
        IIiIii1 = ( o00oo0000 * oo0ooO0 [ ooO0oo ] )
        Oo00O0Oo0Oo = ooO0oo0o0 + IIiIii1
        if ( II1i > Oo00O0Oo0Oo ) :
         II1i -= Oo00O0Oo0Oo
         I1ii1II1iII [ iIIIIiiIii ] = float ( ooO0oo0o0 )
         I1ii1II1iII [ ooO0oo ] = float ( IIiIii1 )
        else :
         I1ii1II1iII [ iIIIIiiIii ] = float ( II1i / 2 )
         I1ii1II1iII [ ooO0oo ] = float ( II1i / 2 )
         II1i = 0
        print "[XFSE|Nfo] ... Prio #" + str ( i1 ) + ": Fill Tank #" + str ( iIIIIiiIii ) + " to " + str ( int ( I1ii1II1iII [ iIIIIiiIii ] / self . constKgToGal ) ) + " (of " + str ( int ( ooO0oo0o0 / self . constKgToGal ) ) + ") gal"
        print "[XFSE|Nfo]          ... Fill Tank #" + str ( ooO0oo ) + " to " + str ( int ( I1ii1II1iII [ ooO0oo ] / self . constKgToGal ) ) + " (of " + str ( int ( IIiIii1 / self . constKgToGal ) ) + ") gal"
       else :
        print "[XFSE|ERR] ... Error reading fuel tank config line!"
        if 62 - 62: iIii1I11I1II1 + iII111i . Oo0Ooo / IiII % O0 . I1Ii111
    if ( II1i > 0 ) :
     print "[XFSE|WRN] " + str ( int ( II1i / self . constKgToGal ) ) + " gal don't fit into the tanks and will be lost!"
     if 93 - 93: i11iIiiIii % iIii1I11I1II1 % i11iIiiIii + o0oOOo0O0Ooo / o0oOOo0O0Ooo / II111iiii
    print "[XFSE|Nfo] --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---"
    if 49 - 49: OOooOOo . I1ii11iIi11i . i11iIiiIii - II111iiii / Ii1I
    if 62 - 62: OOooOOo
    XPLMSetDatavf ( XPLMFindDataRef ( "sim/flightmodel/weight/m_fuel" ) , I1ii1II1iII , 0 , I1iI1I1 )
    if 1 - 1: IiII / IiII - i11iIiiIii
    if 87 - 87: Oo0Ooo / O0 * IiII / o0oOOo0O0Ooo
    if 19 - 19: I1Ii111 + i1IIi . I1IiiI - Oo0Ooo
    self . checkfuel = XPLMGetDataf ( XPLMFindDataRef ( "sim/flightmodel/weight/m_fuel_total" ) )
    iIi1I1 = int ( ( XPLMGetDataf ( XPLMFindDataRef ( "sim/flightmodel/weight/m_fuel_total" ) ) * 0.3721 ) + 0.5 )
    if 63 - 63: iII111i * I1ii11iIi11i . OoooooooOO / OOooOOo * Oo0Ooo . ooOoO0o
    XPSetWidgetProperty ( self . StartFlyButton , xpProperty_Enabled , 0 )
    XPHideWidget ( self . StartFlyButton )
    XPSetWidgetProperty ( self . EndFlyButton , xpProperty_Enabled , 0 )
    XPShowWidget ( self . EndFlyButton )
    XPSetWidgetProperty ( self . CancelFlyButton , xpProperty_Enabled , 1 )
    if 62 - 62: i1IIi / ooOoO0o . I1IiiI * o0oOOo0O0Ooo
    self . Arrived = 0
    self . flightStart = int ( XPLMGetDataf ( XPLMFindDataRef ( "sim/time/total_flight_time_sec" ) ) )
    self . flightTimerLast = self . flightTimer
    self . flightTime = 0
    self . flying = 1
    self . airborne = 0
    self . gsCheat = 0
    if 21 - 21: o0oOOo0O0Ooo
    XPSetWidgetDescriptor ( self . ServerResponseCaption , "" )
    if 81 - 81: I11i / iIii1I11I1II1 - ooOoO0o * I1Ii111 . I1IiiI * I1ii11iIi11i
    if ( II1i == 0 ) :
     self . setInfoMessage ( "Your flight has been started:" ,
 str ( iIi1I1 ) + " gallons of fuel onboard." ,
 str ( OoOOO0o0OOO ) + " assignments loaded." ,
 "Enjoy your flight!" ,
 "green" )
    else :
     self . setInfoMessage ( "Your flight has been started:" ,
 "Only " + str ( iIi1I1 ) + " of " + str ( int ( OoOo / self . constKgToGal ) ) + " gallons of fuel onboard. WARNING: " + str ( int ( II1i / self . constKgToGal ) ) + " gal will be lost!" ,
 str ( OoOOO0o0OOO ) + " assignments loaded." ,
 "Enjoy your flight!" ,
 "yellow" )
     if 95 - 95: I1IiiI
    for O0OOO000o0o in range ( self . NumberOfEngines ) :
     self . ACEngine [ O0OOO000o0o ] . clearEng ( )
     if 19 - 19: II111iiii - i1IIi - OOooOOo / OOooOOo + OoOoOO00
   return 1
   if 51 - 51: Oo0Ooo % OoOoOO00 * OoooooooOO . i11iIiiIii
   if 77 - 77: II111iiii
   if 42 - 42: Ii1I * I1Ii111 . IiII * I1IiiI + OoOoOO00
   if 25 - 25: I11i . I1IiiI + oO0o
   if 75 - 75: IiII - o0oOOo0O0Ooo % iII111i + i11iIiiIii
   if 100 - 100: I11i + o0oOOo0O0Ooo - i11iIiiIii - II111iiii
   if 40 - 40: OoOoOO00 % OoO0O00
   if 62 - 62: o0oOOo0O0Ooo
   if 15 - 15: I11i + Ii1I . OOooOOo * OoO0O00 . OoOoOO00
 def endFly ( self ) :
  if ( self . airborne < 2 ) :
   print "[XFSE|WRN] End flight function is disabled"
  else :
   print "[XFSE|dbg] End flight function"
   print "[XFSE|Nfo] Aircraft (Plane or Heli) arrived"
   if 18 - 18: i1IIi % II111iiii + I1Ii111 % Ii1I
   if self . Arrived == 0 :
    if self . leaseTime > 0 :
     if 72 - 72: iIii1I11I1II1
     print "[XFSE|Nfo] Flight has arrived"
     if 45 - 45: Oo0Ooo - o0oOOo0O0Ooo % I1Ii111
     if 38 - 38: I1Ii111 % OOooOOo - OoooooooOO
     XPSetWidgetDescriptor ( self . EndFlightCaption , "Flight has ended ..." )
     if 87 - 87: OoO0O00 % I1IiiI
     self . Transmitting = self . Transmitting + 1
     XPSetWidgetDescriptor ( self . ServerResponseCaption , "Transmitting (Try " + str ( self . Transmitting ) + ") ..." )
     if ( self . Transmitting == 2 ) :
      XPShowWidget ( self . XFSEWidget )
      if 77 - 77: iIii1I11I1II1 - i1IIi . oO0o
     iIi1iIIIiIiI = XPLMFindDataRef ( "sim/flightmodel/position/latitude" )
     OooOo000o0o = XPLMFindDataRef ( "sim/flightmodel/position/longitude" )
     iI1I1iII1i = XPLMGetDataf ( iIi1iIIIiIiI )
     iiIIii = XPLMGetDataf ( OooOo000o0o )
     if 70 - 70: o0oOOo0O0Ooo - OOooOOo
     if 62 - 62: I11i
     O000oOo = 0
     I1iI1I1 = 9
     if 53 - 53: iIii1I11I1II1 + o0oOOo0O0Ooo - OoOoOO00 - oO0o / ooOoO0o % i11iIiiIii
     I11 = [ ]
     XPLMGetDatavf ( XPLMFindDataRef ( "sim/flightmodel/weight/m_fuel" ) , I11 , 0 , I1iI1I1 )
     for oOOooo in range ( I1iI1I1 ) :
      O000oOo = O000oOo + I11 [ oOOooo ] / float ( 2.68735 )
      if 80 - 80: I1IiiI - i11iIiiIii
     print "[XFSE|Nfo] Fuel at arrival: " + str ( O000oOo )
     if 69 - 69: oO0o % OoooooooOO . I1IiiI
     if 34 - 34: Ii1I * OoOoOO00 - IiII - I1IiiI - Ii1I
     Ii1iIi111I1i = float ( 0 )
     for I1III111i in range ( len ( self . FuelTanks ) ) :
      if self . FuelTanks [ I1III111i ] == 1 :
       Ii1iIi111I1i = Ii1iIi111I1i + 1
       if 4 - 4: i1IIi + ooOoO0o + i1IIi
       if 31 - 31: Ii1I
     OoOOo00 = [ ]
     O00 = O000oOo / float ( Ii1iIi111I1i )
     for I1III111i in range ( len ( self . FuelTanks ) ) :
      if self . FuelTanks [ I1III111i ] == 0 :
       OoOOo00 . append ( 0 )
      else :
       OoOOo00 . append ( O00 )
       if 94 - 94: I11i . I11i + i11iIiiIii - OOooOOo * I1ii11iIi11i
     iIoo0ooooO = OoOOo00 [ 0 ]
     iiIIi = OoOOo00 [ 1 ]
     i1i = OoOOo00 [ 2 ]
     iIIiI1iiI = OoOOo00 [ 3 ]
     I11Ii111I = OoOOo00 [ 4 ]
     Oo00OO0 = OoOOo00 [ 5 ]
     oo0OoO00OoOOOo = OoOOo00 [ 6 ]
     Oo0 = OoOOo00 [ 7 ]
     o0OOOOO0O = OoOOo00 [ 8 ]
     I1I1IiIi1 = OoOOo00 [ 9 ]
     oOO0o0oo0 = OoOOo00 [ 10 ]
     if 78 - 78: OOooOOo + iII111i . IiII
     if 91 - 91: iIii1I11I1II1 . o0oOOo0O0Ooo . I1ii11iIi11i + OoooooooOO
     o0o0O0Oo = ""
     IiiIIi = int ( self . flightTime )
     for O00o0O in range ( self . NumberOfEngines ) :
      o0o0O0Oo = o0o0O0Oo + str ( self . ACEngine [ O00o0O ] . getData ( IiiIIi ) )
      if 32 - 32: o0oOOo0O0Ooo + OoOoOO00 - OoooooooOO
     print "[XFSE|Nfo] Engine conditions: " + o0o0O0Oo
     if 39 - 39: OoooooooOO * OOooOOo * O0 . I11i . OoO0O00 + ooOoO0o
     print "[XFSE|Nfo] Sending flight to the server ..."
     if 9 - 9: OoOoOO00 + oO0o % OoooooooOO + o0oOOo0O0Ooo
     ooOO0o = self . XFSEpost ( "user=" + self . userstr + "&pass=" + self . passstr + "&action=arrive&rentalTime=" + str ( IiiIIi ) + "&lat=" + str ( iI1I1iII1i ) + "&lon=" + str ( iiIIii ) + "&c=" + str ( iIoo0ooooO ) + "&lm=" + str ( iiIIi ) + "&la=" + str ( i1i ) + "&let=" + str ( iIIiI1iiI ) + "&rm=" + str ( I11Ii111I ) + "&ra=" + str ( Oo00OO0 ) + "&rt=" + str ( oo0OoO00OoOOOo ) + "&c2=" + str ( Oo0 ) + "&c3=" + str ( o0OOOOO0O ) + "&x1=" + str ( I1I1IiIi1 ) + "&x2=" + str ( oOO0o0oo0 ) + o0o0O0Oo )
     if 51 - 51: Oo0Ooo - I1ii11iIi11i * I11i
     if len ( ooOO0o . getElementsByTagName ( 'result' ) ) > 0 :
      if 12 - 12: iIii1I11I1II1 % ooOoO0o % ooOoO0o
      O00O00O000OOO = ooOO0o . getElementsByTagName ( 'result' ) [ 0 ] . firstChild . data
      print "[XFSE|Nfo] Server returned: " + O00O00O000OOO
      if 78 - 78: IiII . OoOoOO00 . I11i
      if 97 - 97: oO0o
      O00O00O000OOO = O00O00O000OOO . replace ( '|' , ' ' )
      if 80 - 80: I1IiiI . Ii1I
      if 47 - 47: I11i + ooOoO0o + II111iiii % i11iIiiIii
      OOoOoo00Oo = O00O00O000OOO . split ( ' ' )
      if 9 - 9: II111iiii * II111iiii . i11iIiiIii * iIii1I11I1II1
      for II1I11Iii1 in range ( len ( OOoOoo00Oo ) - 1 ) :
       OOoOoo00Oo [ II1I11Iii1 ] = OOoOoo00Oo [ II1I11Iii1 ] + " "
       if 16 - 16: Ii1I * OoO0O00 / oO0o
       if 22 - 22: oO0o + iIii1I11I1II1 % Oo0Ooo / I11i / Ii1I
      II1I11Iii1 = 0
      while II1I11Iii1 < len ( OOoOoo00Oo ) - 1 :
       if ( len ( OOoOoo00Oo [ II1I11Iii1 ] ) + len ( OOoOoo00Oo [ II1I11Iii1 + 1 ] ) <= 80 ) :
        OOoOoo00Oo [ II1I11Iii1 ] = OOoOoo00Oo [ II1I11Iii1 ] + OOoOoo00Oo . pop ( II1I11Iii1 + 1 )
       else :
        II1I11Iii1 = II1I11Iii1 + 1
        if 54 - 54: OoOoOO00 % IiII . i11iIiiIii
        if 93 - 93: ooOoO0o % i11iIiiIii % I1Ii111
      for II1I11Iii1 in range ( len ( OOoOoo00Oo ) ) :
       OOoOoo00Oo [ II1I11Iii1 ] = OOoOoo00Oo [ II1I11Iii1 ] . strip ( ) ;
       if 64 - 64: I1Ii111 + I1IiiI * O0 / Oo0Ooo - I11i % I11i
       if 59 - 59: OOooOOo + OoooooooOO
      OoooOooooo0 = 4 - len ( OOoOoo00Oo )
      for II1I11Iii1 in range ( OoooOooooo0 ) :
       OOoOoo00Oo . append ( "" )
       if 66 - 66: oO0o * iIii1I11I1II1 % iIii1I11I1II1 * IiII - ooOoO0o - IiII
      if ( OOoOoo00Oo [ 0 ] . find ( "Your flight is logged and the results can be found at the website" ) == 0 ) :
       self . errorcolor = "green"
       if 70 - 70: I1Ii111 + oO0o
       IioO0O = self . flightTime / 3600
       oOO = ( self . flightTime - IioO0O * 3600 ) / 60
       iIi1I1 = int ( ( XPLMGetDataf ( XPLMFindDataRef ( "sim/flightmodel/weight/m_fuel_total" ) ) * 0.3721 ) + 0.5 )
       if 93 - 93: I1Ii111 + Ii1I
       i1i1 = str ( IioO0O )
       if ( IioO0O < 10 ) :
        i1i1 = "0" + i1i1
        if 27 - 27: I1Ii111 + OoooooooOO - OoOoOO00
       II11 = str ( oOO )
       if ( oOO < 10 ) :
        II11 = "0" + II11
        if 11 - 11: I1ii11iIi11i
       self . setInfoMessage ( OOoOoo00Oo [ 0 ] ,
 OOoOoo00Oo [ 1 ] ,
 OOoOoo00Oo [ 2 ] ,
 "Total Flight time " + i1i1 + ":" + II11 + ". Still " + str ( iIi1I1 ) + " gallons of fuel onboard." ,
 "green" )
      else :
       self . setInfoMessage ( OOoOoo00Oo [ 0 ] ,
 OOoOoo00Oo [ 1 ] ,
 OOoOoo00Oo [ 2 ] ,
 OOoOoo00Oo [ 3 ] ,
 "red" )
       if 26 - 26: iIii1I11I1II1 * I1Ii111 - OOooOOo
      XPSetWidgetProperty ( self . StartFlyButton , xpProperty_Enabled , 1 )
      XPShowWidget ( self . StartFlyButton )
      XPSetWidgetProperty ( self . EndFlyButton , xpProperty_Enabled , 0 )
      XPHideWidget ( self . EndFlyButton )
      XPSetWidgetProperty ( self . CancelFlyButton , xpProperty_Enabled , 0 )
      XPSetWidgetDescriptor ( self . CancelFlyButton , self . CancelFlyButtonString1 )
      if 27 - 27: I1ii11iIi11i * I1Ii111 - OoO0O00 + Ii1I * Ii1I
      self . flying = 0
      self . airborne = 0
      self . Arrived = 1
      if 55 - 55: ooOoO0o
      print "[XFSE|dbg] Flight time reset. All instruments enabled"
      self . flightStart = 0
      self . flightTime = 0
      self . enableAllInstruments ( )
      self . stPayload = 0
      self . setPlanePayload ( self . stPayload )
      if 82 - 82: I1Ii111 - OOooOOo + OoO0O00
      XPSetWidgetDescriptor ( self . ServerResponseCaption , "Transmitting (Try " + str ( self . Transmitting ) + ") ... OK" )
     else :
      print "[XFSE|WRN] Flight logging NOT complete. Check your internet connection to the FSE-Server and try again."
      if 64 - 64: o0oOOo0O0Ooo . O0 * Ii1I + OoooooooOO - Oo0Ooo . OoooooooOO
    else :
     print "[XFSE|Nfo] Lease time has ended, cancelling flight"
     self . cancelFlight ( "Lease time has ended. Your flight has been cancelled. Sorry, you will have to re-fly this trip" , "" )
     if 70 - 70: Oo0Ooo - oO0o . iIii1I11I1II1 % I11i / OoOoOO00 - O0
     if 55 - 55: iII111i - OoO0O00
     if 100 - 100: O0
 def cancelFlight ( self , message , message2 ) :
  if ( self . flying == 0 ) :
   print "[XFSE|WRN] Cancel flight function (BTN) is disabled"
  else :
   print "[XFSE|dbg] Cancel flight function"
   if 79 - 79: iIii1I11I1II1
   O00oO0o = self . XFSEpost ( "user=" + self . userstr + "&pass=" + self . passstr + "&action=cancel" )
   if ( O00oO0o . getElementsByTagName ( 'response' ) [ 0 ] . firstChild . nodeName == "ok" ) :
    self . flying = 0
    self . airborne = 0
    if 15 - 15: I1Ii111 + I11i . OoooooooOO . i11iIiiIii
    XPSetWidgetProperty ( self . StartFlyButton , xpProperty_Enabled , 1 )
    XPShowWidget ( self . StartFlyButton )
    XPSetWidgetProperty ( self . EndFlyButton , xpProperty_Enabled , 0 )
    XPHideWidget ( self . EndFlyButton )
    XPSetWidgetProperty ( self . CancelFlyButton , xpProperty_Enabled , 0 )
    XPSetWidgetDescriptor ( self . CancelFlyButton , self . CancelFlyButtonString1 )
    if 31 - 31: OoooooooOO + iII111i - OoOoOO00 . i1IIi % iII111i
    self . setInfoMessage ( message ,
 message2 ,
 "" ,
 "" ,
 "red" )
    if 43 - 43: OOooOOo * ooOoO0o / iIii1I11I1II1 - Ii1I * Ii1I
    XPSetWidgetDescriptor ( self . LeaseCaption , "" )
    XPSetWidgetDescriptor ( self . EndFlightCaption , "" )
    if 60 - 60: iIii1I11I1II1 . OOooOOo + I1ii11iIi11i
    self . enableAllInstruments ( )
    self . stPayload = 0
    self . setPlanePayload ( self . stPayload )
    print "[XFSE|dbg] Cancel flight1: [" + message + "][" + message2 + "]"
    if 44 - 44: O0 . oO0o * i11iIiiIii % i11iIiiIii + O0 / OOooOOo
   else :
    print "[XFSE|dbg] Cancel flight was not successful!"
    if 89 - 89: Ii1I % i1IIi % oO0o
    if 53 - 53: oO0o * OoooooooOO . OoOoOO00
    if 96 - 96: I1IiiI % i1IIi . o0oOOo0O0Ooo . O0
 def login ( self ) :
  if ( self . connected == 1 ) :
   print "[XFSE|WRN] login function (BTN) is disabled"
  else :
   iiIiI = [ ]
   XPGetWidgetDescriptor ( self . LoginUserEdit , iiIiI , 256 )
   XPGetWidgetDescriptor ( self . LoginPassEdit , iiIiI , 256 )
   self . userstr = iiIiI [ 0 ]
   self . passstr = iiIiI [ 1 ]
   Ii1Iii11 = self . XFSEpost ( "user=" + self . userstr + "&pass=" + self . passstr + "&action=accountCheck" )
   print "[XFSE|Nfo] Logincheck"
   if 97 - 97: OOooOOo / oO0o . II111iiii
   i1i11i1Ii11 = Ii1Iii11 . getElementsByTagName ( 'response' ) [ 0 ] . firstChild . nodeName
   if 60 - 60: OOooOOo / I1IiiI
   if ( i1i11i1Ii11 == "ok" or i1i11i1Ii11 == "oknotcurrent" ) :
    print "[XFSE|Nfo] Login successful"
    if 78 - 78: I11i . IiII
    if ( i1i11i1Ii11 == "ok" ) :
     XPSetWidgetDescriptor ( self . ServerResponseCaption , "Logged in!" )
     self . setInfoMessage ( "Logged in!" ,
 "" ,
 "" ,
 "" ,
 "green" )
    else :
     XPSetWidgetDescriptor ( self . ServerResponseCaption , "Logged in! Update available!" )
     self . setInfoMessage ( "Logged in!" ,
 "There's a newer version of the client available." ,
 "Please consider updating your files." ,
 "Check the FSE-Forum to get the update." ,
 "yellow" )
     if 38 - 38: OoOoOO00 + IiII
    self . connected = 1
    if 15 - 15: Oo0Ooo + I11i . ooOoO0o - iIii1I11I1II1 / O0 % iIii1I11I1II1
    XPSetWidgetProperty ( self . LoginButton , xpProperty_Enabled , 0 )
    if 86 - 86: I1IiiI / oO0o * Ii1I
    XPSetWidgetProperty ( self . StartFlyButton , xpProperty_Enabled , 1 )
    XPShowWidget ( self . StartFlyButton )
    XPSetWidgetProperty ( self . EndFlyButton , xpProperty_Enabled , 0 )
    XPHideWidget ( self . EndFlyButton )
    XPSetWidgetProperty ( self . CancelFlyButton , xpProperty_Enabled , 0 )
    if 64 - 64: ooOoO0o / O0 * OoOoOO00 * ooOoO0o
   else :
    print "[XFSE|Nfo] Login was not successful"
    if ( i1i11i1Ii11 == "error" ) :
     print "[XFSE|Nfo] Invalid script"
     XPSetWidgetDescriptor ( self . ServerResponseCaption , "Error!" )
     self . setInfoMessage ( Ii1Iii11 . getElementsByTagName ( 'error' ) [ 0 ] . firstChild . data ,
 "" ,
 "" ,
 "" ,
 "red" )
    else :
     if ( i1i11i1Ii11 == "notok" ) :
      print "[XFSE|Nfo] New version avail"
      XPSetWidgetDescriptor ( self . ServerResponseCaption , "Update available!" )
      XPSetWidgetProperty ( self . UpdateButton , xpProperty_Enabled , 1 )
      self . setInfoMessage ( "!!! New version is available: v" + str ( Ii1Iii11 . getElementsByTagName ( 'notok' ) [ 0 ] . firstChild . data ) ,
 "" ,
 "" ,
 "" ,
 "red" )
     else :
      print "[XFSE|Nfo] Invalid account"
      XPSetWidgetDescriptor ( self . ServerResponseCaption , "Invalid account!" )
      self . setInfoMessage ( "Invalid account!" ,
 "" ,
 "" ,
 "" ,
 "red" )
  return 1
  if 60 - 60: I11i / i1IIi % I1ii11iIi11i / I1ii11iIi11i * I1ii11iIi11i . i11iIiiIii
  if 99 - 99: OoOoOO00
  if 77 - 77: o0oOOo0O0Ooo
 def doUpdate ( self ) :
  IIiIi11iiIi = urlopen ( 'http://www.fseconomy.net/download/client/xfse/PI_xfse.py' ) . read ( )
  i11iI11I1I = open ( os . path . join ( 'Resources' , 'plugins' , 'PythonScripts' , 'PI_xfse.py' ) , 'w' )
  i11iI11I1I . write ( IIiIi11iiIi )
  i11iI11I1I . close ( )
  self . setInfoMessage ( "Your client is updated, please restart X-Plane,"
 "or reload plugins via Plugins / Python Interface / Control Panel"
 "" ,
 "" ,
 "yellow" )
  self . errormessage = 100
  if 47 - 47: O0 * I1IiiI * OoO0O00 . II111iiii
  if 95 - 95: Ii1I % IiII . O0 % I1Ii111
  if 68 - 68: Oo0Ooo . Oo0Ooo - I1ii11iIi11i / I11i . ooOoO0o / i1IIi
 def addAssignment ( self , aIndex , aFrom , aTo , aCargo ) :
  print "[XFSE|Nfo] Adding assignment #" + str ( aIndex ) + ", From: " + str ( aFrom ) + ", To: " + str ( aTo ) + ", Cargo: " + str ( aCargo )
  iI1i1iIi1iiII = ( aIndex + 1 ) * 18 + 120
  o0OoO0000o = ( aIndex + 1 ) * 28 + 120
  o0Ii1 = [ ]
  IIi1IiII = [ ]
  o0IIIIiI11I = [ ]
  iiiI11iIIi1 = [ ]
  XPGetWidgetGeometry ( self . XFSEWidget , o0Ii1 , IIi1IiII , o0IIIIiI11I , iiiI11iIIi1 )
  o00OoooooooOo = IIi1IiII [ 0 ]
  iIii1I = o0Ii1 [ 0 ]
  iii11i1 = 10
  self . FromCaption . append ( XPCreateWidget ( iIii1I + 20 , o00OoooooooOo - iI1i1iIi1iiII + iii11i1 * aIndex , iIii1I + 50 , o00OoooooooOo - o0OoO0000o + iii11i1 * aIndex , 1 , "From: -" , 0 , self . XFSEWidget , xpWidgetClass_Caption ) )
  self . ToCaption . append ( XPCreateWidget ( iIii1I + 140 , o00OoooooooOo - iI1i1iIi1iiII + iii11i1 * aIndex , iIii1I + 170 , o00OoooooooOo - o0OoO0000o + iii11i1 * aIndex , 1 , "To: -" , 0 , self . XFSEWidget , xpWidgetClass_Caption ) )
  self . CargoCaption . append ( XPCreateWidget ( iIii1I + 210 , o00OoooooooOo - iI1i1iIi1iiII + iii11i1 * aIndex , iIii1I + 240 , o00OoooooooOo - o0OoO0000o + iii11i1 * aIndex , 1 , "Cargo: -" , 0 , self . XFSEWidget , xpWidgetClass_Caption ) )
  XPSetWidgetDescriptor ( self . FromCaption [ aIndex ] , str ( aFrom ) )
  XPSetWidgetDescriptor ( self . ToCaption [ aIndex ] , str ( aTo ) )
  XPSetWidgetDescriptor ( self . CargoCaption [ aIndex ] , str ( aCargo ) )
  if 48 - 48: ooOoO0o * I1ii11iIi11i
  if 15 - 15: OoO0O00 * I11i % iIii1I11I1II1 * I1ii11iIi11i
# dd678faae9ac167bc83abf78e5cb2f3f0688d3a3
