#########################################################################################################################
# 
# Beta-1 2015/04/05: Teddii
#					- initial version
# 
# Beta-2 2015/04/06: Teddii
#					- fixed bug that prevents saving the config, if file doesn't already exists
# 
# Beta-3 2015/04/06: Teddii
#					- Client window now pops up when config cannot be read OR the config is not valid
#					- checks now look for:
#						- all tanks are assigned
#						- every tank is assigned only once
#						- no disabled tanks assigned
#						- two tanks max per prio level
# 
# Beta-4 2015/04/07: Teddii
#					- hides by default (after loading the script)
# 
# 1.0 2015/06/27: Teddii
#					- Release
# 
# 1.1 2015/08/03: Teddii
#					- removed Fuel Tank Selector Indicators
# 
#########################################################################################################################

from XPStandardWidgets import *
from XPLMProcessing import *
from XPLMDataAccess import *
from XPLMNavigation import *
from XPLMUtilities import *
from XPLMGraphics import * 
from XPWidgetDefs import *
from XPLMDisplay import *
from XPLMMenus import *
from XPWidgets import *
from XPLMPlanes import *
import time
import os

##########################################################################################################################
## the main plugin interface class
class PythonInterface:
	def XPluginStart(self):
		self.Name = "FSE FuelTanksConfig"
		self.Sig =  "Teddii.Python.XFSEconomy"
		self.Desc = "FSE Fuel Tank Configuration for X-Economy"
		self.VERSION="1.1"

		self.priosTot=5

		self.configLoaded=""
		self.savedState=0
		
		self.checkStateCaptionCounter=0
		self.checkStateCaptionCounterTimeout=3
		
		self.constKgToGal=float(2.68735)

		self.edtFuelTankData = [0 for x in range(9)]
		#self.chkFuelTankActive = [0 for x in range(9)]
		
		self.chkFuelTankConfig = [[0 for x in range(9)] for x in range(self.priosTot)]
		
		self.MenuItem1 = 0			#Flag if main window has already been created

		Item = XPLMAppendMenuItem(XPLMFindPluginsMenu(), "FSE-FuelTanksConfig", 0, 1)
		self.XFSEMenuHandlerCB = self.XFSEMenuHandler
		self.Id = XPLMCreateMenu(self, "FSE-FuelTanksConfig" , XPLMFindPluginsMenu(), Item, self.XFSEMenuHandlerCB,	0)
		XPLMAppendMenuItem(self.Id, "Open FSE-FuelTanksConfig", 1, 1)
		XPLMAppendMenuItem(self.Id, "-", 2, 1)
		
		self.updateTimerCB = self.updateTimer
		XPLMRegisterFlightLoopCallback(self, self.updateTimerCB, 1.0, 0)

		self.DrawWindowCB = self.DrawWindowCallback
		self.KeyCB = self.KeyCallback
		self.MouseClickCB = self.MouseClickCallback
		self.WindowId = XPLMCreateWindow(self, 50, 600, 300, 400, 1, self.DrawWindowCB, self.KeyCB, self.MouseClickCB, 0)

		#register Custom commands
		self.CmdWindowShow  = XPLMCreateCommand("fse/fueltanksconfig/show",         "show FSE FuelTanksConfig window")
		self.CmdWindowHide  = XPLMCreateCommand("fse/fueltanksconfig/hide",         "hide FSE FuelTanksConfig window")
		self.CmdWindowTogl  = XPLMCreateCommand("fse/fueltanksconfig/toggle",       "toggle FSE FuelTanksConfig window")
		
		self.CmdWindowShowCB  = self.CmdWindowShowCallback
		self.CmdWindowHideCB  = self.CmdWindowHideCallback
		self.CmdWindowToglCB  = self.CmdWindowToglCallback
		
		XPLMRegisterCommandHandler(self, self.CmdWindowShow,  self.CmdWindowShowCB, 0, 0)
		XPLMRegisterCommandHandler(self, self.CmdWindowHide,  self.CmdWindowHideCB, 0, 0)
		XPLMRegisterCommandHandler(self, self.CmdWindowTogl,  self.CmdWindowToglCB, 0, 0)
		
		#Create the Main Window Widget
		self.CreateXFSEWidget(720, 640)
		self.MenuItem1 = 1
		XPHideWidget(self.XFSEWidget) #comment out for easier debugging
		self.check(0) #reloading file (will show the client window again, if no valid config is found)
		
		#
		return self.Name, self.Sig, self.Desc

	def XPluginStop(self):
		if (self.MenuItem1 == 1):
			XPDestroyWidget(self, self.XFSEWidget, 1)
			self.MenuItem1 = 0

		XPLMDestroyMenu(self, self.Id)
		XPLMUnregisterFlightLoopCallback(self, self.updateTimerCB, 0)
		XPLMDestroyWindow(self, self.WindowId)

		XPLMUnregisterCommandHandler(self, self.CmdWindowShow,  self.CmdWindowShowCB, 0, 0)
		XPLMUnregisterCommandHandler(self, self.CmdWindowHide,  self.CmdWindowHideCB, 0, 0)
		XPLMUnregisterCommandHandler(self, self.CmdWindowTogl,  self.CmdWindowToglCB, 0, 0)
		
		return 1

	def XPluginEnable(self):
		return 1

	def XPluginDisable(self):
		return 1

	def XPluginReceiveMessage(self, inFromWho, inMessage, inParam):
		return 0

	#############################################################
	## Callback for System/plugin calls
	def MouseClickCallback(self, inWindowID, x, y, inMouse, inRefcon):
		return 0

	def KeyCallback(self, inWindowID, inKey, inFlags, inVirtualKey, inRefcon, losingFocus):
		return 0

	def DrawWindowCallback(self, inWindowID, inRefcon):
		return 0

	#############################################################
	## Callback handler for custom commands
	def CmdWindowShowCallback(self, cmd, phase, refcon):
		if(phase==0): #KeyDown event
			print "[XFTC|Nfo] CMD window show"
			XPShowWidget(self.XFSEWidget)
		return 0
			
	def CmdWindowHideCallback(self, cmd, phase, refcon):
		if(phase==0): #KeyDown event
			print "[XFTC|Nfo] CMD window hide"
			XPHideWidget(self.XFSEWidget)
		return 0
			
	def CmdWindowToglCallback(self, cmd, phase, refcon):
		if(phase==0):
			print "[XFTC|Nfo] CMD window toggle"
			if(not XPIsWidgetVisible(self.XFSEWidget)):
				XPShowWidget(self.XFSEWidget)
			else:
				XPHideWidget(self.XFSEWidget)
		return 0
		
	#############################################################
	## GUI Creation Handler
	def CreateXFSEWidget(self, x, y):
		w=620
		h=270
		
		self.globalX=x
		self.globalY=y
		x2 = x + w
		y2 = y - h
		
		#Buffer = "X-Economy v"+str(self.VERSION)+" for X-Plane "+str(self.XPVer)+" ("+str(CompileDate)+")"
		Buffer = "FSE-FuelTanksConfig "+str(self.VERSION)

		# # #####################################################
		# Create the Main Widget window
		self.XFSEWidget = XPCreateWidget(x, y, x2, y2, 1, Buffer, 1,	0, xpWidgetClass_MainWindow)

		# Add Close Box decorations to the Main Widget
		XPSetWidgetProperty(self.XFSEWidget, xpProperty_MainWindowHasCloseBoxes, 1)

		# # #####################################################
		# tank info fields
		top=50
		right=125
		#for tank in range(9):
		#	self.chkFuelTankActive[tank] = (XPCreateWidget(x+20, y-top-(tank*20), x+30, y-top-20-(tank*20),1, "", 0, self.XFSEWidget,xpWidgetClass_Button))
		#	XPSetWidgetProperty(self.chkFuelTankActive[tank], xpProperty_ButtonType, xpRadioButton)
		#	XPSetWidgetProperty(self.chkFuelTankActive[tank], xpProperty_ButtonBehavior, xpButtonBehaviorCheckBox)
		#	XPSetWidgetProperty(self.chkFuelTankActive[tank], xpProperty_Enabled, 0)

		for tank in range(9):
			#with Tank Selectors: self.edtFuelTankData[tank] = (XPCreateWidget(x+50, y-top-(tank*20), x+w-right-30, y-top-20-(tank*20),1, "", 0, self.XFSEWidget,xpWidgetClass_TextField))
			self.edtFuelTankData[tank] = (XPCreateWidget(x+20, y-top-(tank*20), x+w-right-30, y-top-20-(tank*20),1, "", 0, self.XFSEWidget,xpWidgetClass_TextField))
			XPSetWidgetProperty(self.edtFuelTankData[tank], xpProperty_TextFieldType, xpTextEntryField)
			XPSetWidgetProperty(self.edtFuelTankData[tank], xpProperty_Enabled, 0)
			
		#XPCreateWidget(x+15, y-top+30, x+100, y-top+10,1, "Currently selected tanks in loaded X-Plane model", 0, self.XFSEWidget,xpWidgetClass_Caption)
		#XPCreateWidget(x+20, y-top+20, x+100, y-top+0,1, "|", 0, self.XFSEWidget,xpWidgetClass_Caption)

		XPCreateWidget(x+w-right-3, y-top+30, x+w-10, y-top+10,1, "Fuel Tank Priority", 0, self.XFSEWidget,xpWidgetClass_Caption)
		XPCreateWidget(x+w-right, y-top+20, x+w-10, y-top+0,1, "0    1    2    3    4", 0, self.XFSEWidget,xpWidgetClass_Caption)

		XPCreateWidget(x+50, y-top+20-(10*20), x+w-10, y-top+10-(10*20),1, "Changes are written to the config file immediately (no need to save anything)", 0, self.XFSEWidget,xpWidgetClass_Caption)
		self.checkStateCaption = XPCreateWidget(x+50, y-top+10-(10*20), x+w-10, y-top-10-(10*20),1, "", 0, self.XFSEWidget,xpWidgetClass_Caption)

		XPCreateWidget(x+w-right-5, y-top+20-(10*20), x+w-10, y-top+10-(10*20),1, "First >=>=> Last", 0, self.XFSEWidget,xpWidgetClass_Caption)

		for prio in range(self.priosTot):
			for tank in range(9):
				self.chkFuelTankConfig[prio][tank] = (XPCreateWidget(x+w-right+(prio*20), y-top-(tank*20), x+w-right+10+(prio*20), y-top-20-(tank*20),1, "", 0, self.XFSEWidget,xpWidgetClass_Button))
				XPSetWidgetProperty(self.chkFuelTankConfig[prio][tank], xpProperty_ButtonType, xpRadioButton)
				XPSetWidgetProperty(self.chkFuelTankConfig[prio][tank], xpProperty_ButtonBehavior, xpButtonBehaviorCheckBox)

		# # #####################################################
		# reset config button
		self.resetConfigButton = XPCreateWidget(x+w-right-10, y-45-(10*20), x+w-20, y-65-(10*20),1, "RESET config", 0, self.XFSEWidget,xpWidgetClass_Button)
		XPSetWidgetProperty(self.resetConfigButton, xpProperty_ButtonType, xpPushButton)
				
		# # #####################################################
		# Register our widget handler
		self.XFSEHandlerCB = self.XFSEHandler
		XPAddWidgetCallback(self, self.XFSEWidget, self.XFSEHandlerCB)

		# # #####################################################
		# fast-update the contents
		self.check(0)
		
	#############################################################
	## GUI (BTN) Message Handler
	def XFSEHandler(self, inMessage, inWidget, inParam1, inParam2):
		if (inMessage == xpMessage_CloseButtonPushed):
			print "[XFTC|dbg] Client window closed"
			if (self.MenuItem1 == 1):
				XPHideWidget(self.XFSEWidget)
				return 1

		if (inMessage == xpMsg_ButtonStateChanged): # any message from the checkboxes
			if(self.checkConfig()==1):
				self.writeConfig()
			#else: # reset last pressed checkbox
			#	XPSetWidgetProperty(inParam1, xpProperty_ButtonState, 0)
				
		if (inMessage == xpMsg_PushButtonPressed):
			if (inParam1 == self.resetConfigButton):
				self.resetConfig()
				self.writeConfig()
				return 1
			else:
				print "[XFTC|ERR] UNKNOWN GUI button pressed"
				
		return 0

	#############################################################
	## Menu Handler
	def XFSEMenuHandler(self, inMenuRef, inItemRef):
		# If menu selected create our widget dialog
		if (inItemRef == 1):
			if (self.MenuItem1 == 0):
				self.CreateXFSEWidget(50, 540)
				self.MenuItem1 = 1
			else:
				if(not XPIsWidgetVisible(self.XFSEWidget)):
					XPShowWidget(self.XFSEWidget)

		return 0

	#############################################################
	## getPlanePath
	def getPlanePath(self):
		raw_PlanePath = XPLMGetNthAircraftModel(0)
		planePath = os.path.dirname(raw_PlanePath[1])
		return planePath
		
	#############################################################
	## read config file
	def readConfig(self):
		ftcFile = os.path.join(self.getPlanePath(), 'xfse_fueltanks.txt')

		self.resetConfig()
		
		if (os.path.exists(ftcFile) and os.path.isfile(ftcFile)):
			with open(ftcFile, "r") as ins:
				print "[XFTC|Nfo] Reading config "+ftcFile

				tankConfig = []
				for line in ins:
					line=line.strip()
					if(line.find("#")!=0):
						if(len(line)==1 or len(line)==3):
							tankConfig.append(line)
							
				for prio in range(self.priosTot):
					if(prio<len(tankConfig)):
						if(len(tankConfig[prio])>=1):
							tank=int(tankConfig[prio][0]) #first char in line
							print "[XFTC|Nfo] ... Prio"+str(prio)+": Tank #"+str(tank)
							XPSetWidgetProperty(self.chkFuelTankConfig[prio][tank], xpProperty_ButtonState, 1)
						if(len(tankConfig[prio])>=3):
							tank=int(tankConfig[prio][2]) #third char in line
							print "[XFTC|Nfo] ... Prio"+str(prio)+": Tank #"+str(tank)
							XPSetWidgetProperty(self.chkFuelTankConfig[prio][tank], xpProperty_ButtonState, 1)
				print "[XFTC|Nfo] Reading config done ("+str(len(tankConfig))+" lines)"
				return len(tankConfig)
							
		print "[XFTC|ERR] Reading config failed"
		return 0

	#############################################################
	## write config file
	def writeConfig(self):
		planepath=self.getPlanePath()
		ftcFile = os.path.join(planepath, 'xfse_fueltanks.txt')

		if (os.path.exists(planepath)):
			f = open(ftcFile, "w")
			f.write("# XFSE Fuel Tank Config - created by "+self.Name+" "+self.VERSION+"\n")
			f.write("# XFSE Fuel Tank Config - created on "+time.strftime("%c")+"\n")
			for prio in range(self.priosTot):
				line=""
				for tank in range(9):
					checked=XPGetWidgetProperty(self.chkFuelTankConfig[prio][tank], xpProperty_ButtonState, None)
					if(checked==1):
						line+=str(tank)+" "
				if(len(line)>0):
					f.write(line+"\n")
			f.write("# XFSE Fuel Tank Config - EOF\n")
			f.close()

			XPSetWidgetDescriptor(self.checkStateCaption, "The configuration has been saved!")
			self.checkStateCaptionCounter=self.checkStateCaptionCounterTimeout
			print "[XFTC|Nfo] Saving config done"
		else:
			XPSetWidgetDescriptor(self.checkStateCaption, "The path to the configuration does no longer exists!")
			self.checkStateCaptionCounter=self.checkStateCaptionCounterTimeout
			print "[XFTC|ERR] Saving config failed"
		
		return 0
					
	#############################################################
	## reset config (file)
	def resetConfig(self):
		for prio in range(self.priosTot):
			for tank in range(9):
				XPSetWidgetProperty(self.chkFuelTankConfig[prio][tank], xpProperty_ButtonState, 0)
		return 1
					
	#############################################################
	## check checkboxes
	def checkConfig(self):
		tankRatio=[]
		XPLMGetDatavf(XPLMFindDataRef("sim/aircraft/overflow/acf_tank_rat"),tankRatio,0,9)

		numTanks=0
		for tank in range(9):
			if(tankRatio[tank]>0):
				numTanks+=1

		for tank in range(9):
			cntTanks=0 #count every single tank's occurance
			for prio in range(self.priosTot):
				checked=XPGetWidgetProperty(self.chkFuelTankConfig[prio][tank], xpProperty_ButtonState, None)
				if(checked>0):
					cntTanks+=1
					if(cntTanks>1):
						XPSetWidgetDescriptor(self.checkStateCaption, "Every single tank can only be assigned once - CONFIG NOT SAVED!")
						self.checkStateCaptionCounter=self.checkStateCaptionCounterTimeout
						print "[XFTC|WRN] CHECK failed: Tank #"+str(tank)+" is assigned "+str(cntTanks)+" times"
						return 0
				
		cntTanks=0 #count total tanks
		for prio in range(self.priosTot):
			priocnt=0
			for tank in range(9):
				checked=XPGetWidgetProperty(self.chkFuelTankConfig[prio][tank], xpProperty_ButtonState, None)
				if(checked>0):
					cntTanks+=1
					priocnt+=1
					if(tankRatio[tank]==0):
						XPSetWidgetDescriptor(self.checkStateCaption, "You cannot assign a prio level to disabled tanks - CONFIG NOT SAVED!")
						self.checkStateCaptionCounter=self.checkStateCaptionCounterTimeout
						print "[XFTC|WRN] CHECK failed: Tank #"+str(tank)+" is disabled and cannot be assigned"
						return 0
			if(priocnt>2):
				XPSetWidgetDescriptor(self.checkStateCaption, "More than two tanks for prio level "+str(prio)+" assigned - CONFIG NOT SAVED!")
				self.checkStateCaptionCounter=self.checkStateCaptionCounterTimeout
				print "[XFTC|WRN] CHECK failed: More than two (="+str(priocnt)+") tanks assigned in total for prio level "+str(prio)
				return 0
		
		if(cntTanks!=numTanks):
			XPSetWidgetDescriptor(self.checkStateCaption, "Not all tanks assigned (or tanks multipy assigned)- CONFIG NOT SAVED!")
			self.checkStateCaptionCounter=self.checkStateCaptionCounterTimeout
			print "[XFTC|WRN] CHECK failed: Not all tanks are assigned to a prio level"
			return 0
			
		print "[XFTC|WRN] CHECK successfully done"
		return 1

	#############################################################
	## Custom Plane Fueltank Config functions
	def showFuelTankData(self):
		_maxFuel=XPLMGetDataf(XPLMFindDataRef("sim/aircraft/weight/acf_m_fuel_tot"))

		tankRatio=[]
		XPLMGetDatavf(XPLMFindDataRef("sim/aircraft/overflow/acf_tank_rat"),tankRatio,0,9)

		curFuel=[]
		XPLMGetDatavf(XPLMFindDataRef("sim/flightmodel/weight/m_fuel"),curFuel,0,9)
		
		#tankSel = []
		#XPLMGetDatavi(XPLMFindDataRef("sim/cockpit2/fuel/fuel_tank_pump_on"),tankSel,0,9)
		
		for tank in range(9):
			if(tankRatio[tank]>0):
				tankTotGal=int(tankRatio[tank]*_maxFuel/self.constKgToGal)
				tankCurGal=int(curFuel[tank]/self.constKgToGal)
				text="Tank #"+str(tank)+": Capacity: "
				text+=str(int(tankRatio[tank]*100))+"% of "+str(int(_maxFuel/self.constKgToGal))+"gal = "
				text+=str(tankTotGal)+"gal (cur. Level: "
				text+=str(tankCurGal)+"gal="
				text+=str(int(tankCurGal*100/tankTotGal))+"%) "
			else: 
				text=""
			XPSetWidgetDescriptor(self.edtFuelTankData[tank], text)
	
		#for tank in range(9):
		#	if(tankRatio[tank]>0):
		#		if(tankSel[tank]>0):
		#			XPSetWidgetProperty(self.chkFuelTankActive[tank], xpProperty_ButtonState, 1)
		#		else:
		#			XPSetWidgetProperty(self.chkFuelTankActive[tank], xpProperty_ButtonState, 0)

		return 0

	#############################################################
	## timer callback
	def check(self, forceReload):
		if(forceReload!=0):
			self.configLoaded=""
			print "[XFTC|Nfo] UPD: ======================= FORCED reload triggered ..."
			
		currentPlanePath=self.getPlanePath()
		
		if(self.configLoaded!=currentPlanePath):
			print "[XFTC|Nfo] UPD: PlanePath has changed ..."
			print "[XFTC|Nfo] UPD: PlanePath from: "+self.configLoaded
			print "[XFTC|Nfo] UPD: PlanePath  to : "+currentPlanePath
			self.configLoaded=currentPlanePath
			if(self.readConfig()==0):
				XPShowWidget(self.XFSEWidget)
				print "[XFTC|WRN] UPD: read config failed - showing Widget"
			else:
				if(self.checkConfig()==0):
					XPShowWidget(self.XFSEWidget)
					self.resetConfig()
					print "[XFTC|WRN] UPD: check failed - resetting config to defaults and showing Widget"
				else:
					print "[XFTC|WRN] UPD: check ok - keeping current Widget state"
		
		self.showFuelTankData()
		
	#############################################################
	## timer callback
	def updateTimer(self, elapsedMe, elapsedSim, counter, refcon):
		state=XPIsWidgetVisible(self.XFSEWidget)
		if(state and self.savedState==0):
			self.savedState=1
			self.check(1) #force reloading file
		if(not state and self.savedState==1):
			self.savedState=0

		self.check(0)
		
		if(self.checkStateCaptionCounter==0):
			XPSetWidgetDescriptor(self.checkStateCaption, "")
		elif(self.checkStateCaptionCounter>0):
			self.checkStateCaptionCounter-=1
		
		return float(1) # call again in xxx seconds

	#The End
