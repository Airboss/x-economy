from XPStandardWidgets import *
from XPLMProcessing import *
from XPLMDataAccess import *
from XPLMNavigation import *
from XPLMUtilities import *
from XPLMGraphics import * 
from XPWidgetDefs import *
from XPLMDisplay import *
from XPLMMenus import *
from XPWidgets import *
from XPLMPlanes import *

##########################################################################################################################
## the main plugin interface class
class PythonInterface:
	def XPluginStart(self):
		self.Name = "FSE Interface"
		self.Sig =  "Teddii.Python.XFSEconomy"
		self.Desc = "FSE Interface for X-Economy"
		self.VERSION="1.0"

		self.EventState=0
		
		self.fseConnected=0
		self.fseFlying=0
		self.fseAirborne=0
		self.CancelFlyMarker=0

		self.MenuItem1 = 0			#Flag if main window has already been created

		Item = XPLMAppendMenuItem(XPLMFindPluginsMenu(), "FSE-Interface", 0, 1)
		self.XFSEMenuHandlerCB = self.XFSEMenuHandler
		self.Id = XPLMCreateMenu(self, "FSE-Interface" , XPLMFindPluginsMenu(), Item, self.XFSEMenuHandlerCB,	0)
		XPLMAppendMenuItem(self.Id, "Open FSE-Interface", 1, 1)
		XPLMAppendMenuItem(self.Id, "-", 2, 1)
		
		self.checkACStateCB = self.checkACState
		XPLMRegisterFlightLoopCallback(self, self.checkACStateCB, 1.0, 0)

		self.DrawWindowCB = self.DrawWindowCallback
		self.KeyCB = self.KeyCallback
		self.MouseClickCB = self.MouseClickCallback
		self.WindowId = XPLMCreateWindow(self, 50, 600, 300, 400, 1, self.DrawWindowCB, self.KeyCB, self.MouseClickCB, 0)

		#register Custom commands
		self.CmdWindowShow  = XPLMCreateCommand("fse/interface/show",         "show FSE Interface window")
		self.CmdWindowHide  = XPLMCreateCommand("fse/interface/hide",         "hide FSE Interface window")
		self.CmdWindowTogl  = XPLMCreateCommand("fse/interface/toggle",       "toggle FSE Interface window")
		
		self.CmdWindowShowCB  = self.CmdWindowShowCallback
		self.CmdWindowHideCB  = self.CmdWindowHideCallback
		self.CmdWindowToglCB  = self.CmdWindowToglCallback
		
		XPLMRegisterCommandHandler(self, self.CmdWindowShow,  self.CmdWindowShowCB, 0, 0)
		XPLMRegisterCommandHandler(self, self.CmdWindowHide,  self.CmdWindowHideCB, 0, 0)
		XPLMRegisterCommandHandler(self, self.CmdWindowTogl,  self.CmdWindowToglCB, 0, 0)
		
		#Create the Main Window Widget
		self.CreateXFSEWidget(50, 540)
		self.MenuItem1 = 1
		#XPHideWidget(self.XFSEWidget)
		
		#
		return self.Name, self.Sig, self.Desc

	def XPluginStop(self):
		if (self.MenuItem1 == 1):
			XPDestroyWidget(self, self.XFSEWidget, 1)
			self.MenuItem1 = 0

		XPLMDestroyMenu(self, self.Id)
		XPLMUnregisterFlightLoopCallback(self, self.checkACStateCB, 0)
		XPLMDestroyWindow(self, self.WindowId)

		XPLMUnregisterCommandHandler(self, self.CmdWindowShow,  self.CmdWindowShowCB, 0, 0)
		XPLMUnregisterCommandHandler(self, self.CmdWindowHide,  self.CmdWindowHideCB, 0, 0)
		XPLMUnregisterCommandHandler(self, self.CmdWindowTogl,  self.CmdWindowToglCB, 0, 0)
		
		return 1

	def XPluginEnable(self):
		return 1

	def XPluginDisable(self):
		return 1

	def XPluginReceiveMessage(self, inFromWho, inMessage, inParam):
		return 0

	#############################################################
	## Callback for System/plugin calls
	def MouseClickCallback(self, inWindowID, x, y, inMouse, inRefcon):
		return 0

	def KeyCallback(self, inWindowID, inKey, inFlags, inVirtualKey, inRefcon, losingFocus):
		return 0

	def DrawWindowCallback(self, inWindowID, inRefcon):
		return 0

	#############################################################
	## Callback handler for custom commands
	def CmdServerConnCallback(self, cmd, phase, refcon):
		if(phase==0): #KeyDown event
			print "[FseI|Nfo] CMD server connect"
			self.login()
		return 0
			
	def CmdWindowShowCallback(self, cmd, phase, refcon):
		if(phase==0): #KeyDown event
			print "[FseI|Nfo] CMD window show"
			XPShowWidget(self.XFSEWidget)
		return 0
			
	def CmdWindowHideCallback(self, cmd, phase, refcon):
		if(phase==0): #KeyDown event
			print "[FseI|Nfo] CMD window hide"
			XPHideWidget(self.XFSEWidget)
		return 0
			
	def CmdWindowToglCallback(self, cmd, phase, refcon):
		if(phase==0):
			print "[FseI|Nfo] CMD window toggle"
			if(not XPIsWidgetVisible(self.XFSEWidget)):
				XPShowWidget(self.XFSEWidget)
			else:
				XPHideWidget(self.XFSEWidget)
		return 0
		
	#############################################################
	## GUI Creation Handler
	def CreateXFSEWidget(self, x, y):
		w=170
		h=120
		
		self.globalX=x
		self.globalY=y
		x2 = x + w
		y2 = y - h
		
		#Buffer = "X-Economy v"+str(self.VERSION)+" for X-Plane "+str(self.XPVer)+" ("+str(CompileDate)+")"
		Buffer = "FSE-Interface "+str(self.VERSION)

		# # #####################################################
		# Create the Main Widget window
		self.XFSEWidget = XPCreateWidget(x, y, x2, y2, 1, Buffer, 1,	0, xpWidgetClass_MainWindow)

		# Add Close Box decorations to the Main Widget
		XPSetWidgetProperty(self.XFSEWidget, xpProperty_MainWindowHasCloseBoxes, 1)

		# # #####################################################
		# client toggle button
		self.ClientToggleButton = XPCreateWidget(x+5, y-25, x+w-5, y-35,1, "Toggle Client Window", 0, self.XFSEWidget,xpWidgetClass_Button)
		XPSetWidgetProperty(self.ClientToggleButton, xpProperty_ButtonType, xpPushButton)

		# # #####################################################
		# toggle flight button
		self.ToggleFlightButton = XPCreateWidget(x+5, y-40, x+w-5, y-60,1, "Start flight", 0, self.XFSEWidget,xpWidgetClass_Button)
		XPSetWidgetProperty(self.ToggleFlightButton, xpProperty_ButtonType, xpPushButton)
		XPSetWidgetProperty(self.ToggleFlightButton, xpProperty_Enabled, 0)

		# status text
		self.Text1=XPCreateWidget(x+5, y-60, x+(w/2)-5, y-70,1, "Status:",          0, self.XFSEWidget,xpWidgetClass_Caption)
		self.Text2=XPCreateWidget(x+5, y-70, x+(w/2)-5, y-80,1, "Flight Time:",     0, self.XFSEWidget,xpWidgetClass_Caption)
		self.Text3=XPCreateWidget(x+5, y-80, x+(w/2)-5, y-90,1, "Lease Time Left:", 0, self.XFSEWidget,xpWidgetClass_Caption)

		self.Text1=XPCreateWidget(x+(w/2)+15, y-60, x+w-5, y-70,1, "", 0, self.XFSEWidget,xpWidgetClass_Caption)
		self.Text2=XPCreateWidget(x+(w/2)+15, y-70, x+w-5, y-80,1, "", 0, self.XFSEWidget,xpWidgetClass_Caption)
		self.Text3=XPCreateWidget(x+(w/2)+15, y-80, x+w-5, y-90,1, "", 0, self.XFSEWidget,xpWidgetClass_Caption)
        
		# Login button
		self.LoginButton = XPCreateWidget(x+5, y-95, x+(w/2)-5, y-115,1, "Log in", 0, self.XFSEWidget,xpWidgetClass_Button)
		XPSetWidgetProperty(self.LoginButton, xpProperty_ButtonType, xpPushButton)
		XPSetWidgetProperty(self.LoginButton, xpProperty_Enabled, 0)
		# cancel fly button
		self.CancelFlyButton = XPCreateWidget(x+(w/2)+5, y-95, x+w-5, y-115,1, "Cancel flight", 0, self.XFSEWidget,xpWidgetClass_Button)
		XPSetWidgetProperty(self.CancelFlyButton, xpProperty_ButtonType, xpPushButton)
		XPSetWidgetProperty(self.CancelFlyButton, xpProperty_Enabled, 0)

		# # #####################################################
		# Register our widget handler
		self.XFSEHandlerCB = self.XFSEHandler
		XPAddWidgetCallback(self, self.XFSEWidget, self.XFSEHandlerCB)

	#############################################################
	## GUI (BTN) Message Handler
	def XFSEHandler(self, inMessage, inWidget, inParam1, inParam2):
		if (inMessage == xpMessage_CloseButtonPushed):
			print "[FseI|dbg] Client window closed"
			if (self.MenuItem1 == 1):
				XPHideWidget(self.XFSEWidget)
				return 1

		if (inMessage == xpMsg_PushButtonPressed):
			if   (inParam1 == self.LoginButton):
				XPLMCommandOnce(XPLMFindCommand("fse/server/connect"))
				return 1
			elif (inParam1 == self.ClientToggleButton):
				XPLMCommandOnce(XPLMFindCommand("fse/window/toggle"))
				return 1
			elif (inParam1 == self.ToggleFlightButton):
				if(self.fseFlying==0):
					XPLMCommandOnce(XPLMFindCommand("fse/flight/start"))
				else:
					XPLMCommandOnce(XPLMFindCommand("fse/flight/finish"))
				return 1
			elif (inParam1 == self.CancelFlyButton):
				if(self.CancelFlyMarker==0):
					self.CancelFlyMarker=1
					XPSetWidgetDescriptor(self.CancelFlyButton, "Really sure?")
					XPLMCommandOnce(XPLMFindCommand("fse/flight/cancelArm"))
				else:
					XPLMCommandOnce(XPLMFindCommand("fse/flight/cancelConfirm"))
				return 1
			else:
				print "[FseI|ERR] UNKNOWN GUI button pressed"
				
		return 0

	#############################################################
	## Menu Handler
	def XFSEMenuHandler(self, inMenuRef, inItemRef):
		# If menu selected create our widget dialog
		if (inItemRef == 1):
			if (self.MenuItem1 == 0):
				self.CreateXFSEWidget(50, 540)
				self.MenuItem1 = 1
			else:
				if(not XPIsWidgetVisible(self.XFSEWidget)):
					XPShowWidget(self.XFSEWidget)

		
	#############################################################
	## timer callback
	def checkACState(self, elapsedMe, elapsedSim, counter, refcon):
		self.fseConnected 	= XPLMGetDatai(XPLMFindDataRef("fse/status/connected"))
		self.fseFlying	 	= XPLMGetDatai(XPLMFindDataRef("fse/status/flying"))
		self.fseAirborne	= XPLMGetDatai(XPLMFindDataRef("fse/status/airborne"))

		xpHeightAgl=XPLMGetDataf(XPLMFindDataRef("sim/flightmodel/position/y_agl"))
		xpGroundSpd=XPLMGetDataf(XPLMFindDataRef("sim/flightmodel/position/groundspeed"))

		#show/hide events
		if(self.EventState==0):
			if(xpHeightAgl>15 and xpGroundSpd>15 and self.fseAirborne>0):
				self.EventState=1
				if(XPIsWidgetVisible(self.XFSEWidget)):
					XPHideWidget(self.XFSEWidget)
		if(self.EventState==1):
			if(xpHeightAgl<10 and xpGroundSpd<5 and self.fseAirborne>0):
				self.EventState=0
				if(not XPIsWidgetVisible(self.XFSEWidget)):
					XPShowWidget(self.XFSEWidget)

		# calc times
		if(self.fseFlying==0):
			XPSetWidgetDescriptor(self.Text2, "--:--")
			XPSetWidgetDescriptor(self.Text3, "--:--")
		else:
			seconds=XPLMGetDatai(XPLMFindDataRef("fse/status/flighttime"))
			m, s = divmod(seconds, 60)
			h, m = divmod(m, 60)
			XPSetWidgetDescriptor(self.Text2, "%02d:%02d:%02d" % (h, m, s))
			#
			seconds=XPLMGetDatai(XPLMFindDataRef("fse/status/leasetime"))
			m, s = divmod(seconds, 60)
			h, m = divmod(m, 60)
			XPSetWidgetDescriptor(self.Text3, "%d:%02d:%02d" % (h, m, s))

		# button logic
		if(self.fseConnected==0):
			XPSetWidgetDescriptor(self.Text1, "offline")
			XPSetWidgetProperty(self.LoginButton, xpProperty_Enabled, 1)
			XPSetWidgetProperty(self.ToggleFlightButton, xpProperty_Enabled, 0)
			XPSetWidgetProperty(self.CancelFlyButton, xpProperty_Enabled, 0)
		else:
			XPSetWidgetProperty(self.LoginButton, xpProperty_Enabled, 0)
			if(self.fseFlying==0):
				XPSetWidgetDescriptor(self.Text1, "on ground")
				XPSetWidgetProperty(self.ToggleFlightButton, xpProperty_Enabled, 1)
				XPSetWidgetDescriptor(self.ToggleFlightButton, "Start flight")
				XPSetWidgetProperty(self.CancelFlyButton, xpProperty_Enabled, 0)
				XPSetWidgetDescriptor(self.CancelFlyButton, "Cancel flight")
				self.CancelFlyMarker=0
			else:
				if(self.fseAirborne==0):
					XPSetWidgetDescriptor(self.Text1, "departing")
				else:
					XPSetWidgetDescriptor(self.Text1, "airborne")
				XPSetWidgetProperty(self.CancelFlyButton, xpProperty_Enabled, 1)
				XPSetWidgetDescriptor(self.ToggleFlightButton, "Finish flight")
				if(self.fseAirborne==2):
					XPSetWidgetProperty(self.ToggleFlightButton, xpProperty_Enabled, 1)
				else:
					XPSetWidgetProperty(self.ToggleFlightButton, xpProperty_Enabled, 0)
		
		return float(0.1) # call again in xxx seconds

	#The End
