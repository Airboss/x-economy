# X-Economy

This is a plugin for [X-Plane](http://x-plane.com), to connect and use [FSEconomy](http://www.fseconomy.net/).
The plugin uses Sandy Barbour's [Python interface](http://www.xpluginsdk.org/python_interface.htm), so plugin may work on Mac, Linux and Windows too.

# Download

[Download Latest release](http://server.fseconomy.net/static/x-client/x-economy-current.zip)

# How to install

[How to install](https://sites.google.com/site/fseoperationsguide/getting-started/using-the-fse-client/x-plane#TOC-Installation)

# Modifying your plane so that fse recognizes it

[Modifying your plane](https://sites.google.com/site/fseoperationsguide/getting-started/using-the-fse-client/x-plane#TOC-Modify-your-airplanes)

# Time compression in x-plane

- Time compression in v9 has to be assigned a key. In the Settings > Joystick & Equipment > Keys menu, scroll most of the way to the bottom and find the 'operation/flightmodel_speed_change' line and set an appropriate keystroke. The keystroke will cycle you through x1, x2, x4, x6, x1 and so on. Be advised though that in using time compression the flight model is making bigger jumps between frames and weird plane behavior can result. I recommend that you set the flight models per frame in the 'Settings > Operations and Warnings' menu to at least 2. Also be aware that when using time compression, X-Plane will reduce the visibility to keep the frame rate from getting too low.

# Thanks

Thanks to Andrew, Nikos, Venom and all the X-Plane guys for their help and input :)

# License
The MIT License